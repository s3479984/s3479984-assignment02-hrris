﻿Option Explicit On
Option Strict On

Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports s3479984_Assignment02_HRRIS.Validation

<TestClass()> Public Class TestValidation

    <TestInitialize()> Public Sub setup()
        Debug.Print("Setting up ...")
    End Sub

    <TestCleanup()> Public Sub cleanup()
        Debug.Print("Cleaning up ...")
    End Sub

    'suppose to fail
    <TestMethod()> Public Sub TestIsNumericVal_01()
        Dim oValidation As New s3479984_Assignment02_HRRIS.Validation
        Dim sNumeric = "word"
        Assert.AreEqual(False, oValidation.isNumericVal(sNumeric))
    End Sub

    'suppose to pass
    <TestMethod()> Public Sub TestIsNumericVal_02()
        Dim oValidation As New s3479984_Assignment02_HRRIS.Validation
        Dim sNumeric = "1234"
        Assert.AreEqual(True, oValidation.isNumericVal(sNumeric))
    End Sub

    'suppose to fail
    <TestMethod()> Public Sub TestIsNumericVal_03()
        Dim oValidation As New s3479984_Assignment02_HRRIS.Validation
        Dim sNumeric = "!@#$"
        Assert.AreEqual(False, oValidation.isNumericVal(sNumeric))
    End Sub

    'suppose to fail
    <TestMethod()> Public Sub TestIsAlphaNumericVal_04()
        Dim oValidation As New s3479984_Assignment02_HRRIS.Validation
        Dim sNumeric = "!@#$"
        Assert.AreEqual(False, oValidation.isAlphaNumericVal(sNumeric))
    End Sub

    'suppose to pass
    <TestMethod()> Public Sub TestIsAlphaNumericVal_05()
        Dim oValidation As New s3479984_Assignment02_HRRIS.Validation
        Dim sNumeric = "abc123"
        Assert.AreEqual(True, oValidation.isAlphaNumericVal(sNumeric))
    End Sub

    'suppose to fail, phone is 8 < x > 10 number
    <TestMethod()> Public Sub TestIsPhoneNumber_06()
        Dim oValidation As New s3479984_Assignment02_HRRIS.Validation
        Dim sNumeric = "asdfg"
        Assert.AreEqual(False, oValidation.isPhoneNumber(sNumeric))
    End Sub

    'suppose to pass
    <TestMethod()> Public Sub TestIsPhoneNumber_07()
        Dim oValidation As New s3479984_Assignment02_HRRIS.Validation
        Dim sNumeric = "1234567891"
        Assert.AreEqual(True, oValidation.isPhoneNumber(sNumeric))
    End Sub

    'suppose to fail
    <TestMethod()> Public Sub TestIsPhoneNumber_08()
        Dim oValidation As New s3479984_Assignment02_HRRIS.Validation
        Dim sNumeric = "123456"
        Assert.AreEqual(False, oValidation.isPhoneNumber(sNumeric))
    End Sub

    'suppose to fail
    <TestMethod()> Public Sub TestIsEmail_09()
        Dim oValidation As New s3479984_Assignment02_HRRIS.Validation
        Dim sNumeric = "sejlfseljf"
        Assert.AreEqual(False, oValidation.isEmail(sNumeric))
    End Sub

    'suppose to pass
    <TestMethod()> Public Sub TestIsEmail_10()
        Dim oValidation As New s3479984_Assignment02_HRRIS.Validation
        Dim sNumeric = "sejlfseljf@mail.com"
        Assert.AreEqual(True, oValidation.isEmail(sNumeric))
    End Sub

End Class