﻿Option Explicit On
Option Strict On

Public Class rpt7BreakBookingReport

    Private Sub frmComboBox_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cboMonth.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnGenerateReport.Click
        Dim oController As rpt7BreakBookingReportController = New rpt7BreakBookingReportController

        Dim sMonth = cboMonth.SelectedItem
        Dim sYear = txtYear.Text
        oController.createBreakReport(CStr(sMonth), CStr(sYear))

    End Sub

End Class