﻿Option Explicit On
Option Strict On

Public Class rpt5CheckinDueReport

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnGenerateReport.Click
        Dim oController As rpt5CheckinDueReportController = New rpt5CheckinDueReportController

        Dim sMonth = cboMonth.SelectedItem
        Dim sYear = txtYear.Text
        oController.createReport(CStr(sMonth), CStr(sYear))

    End Sub

End Class