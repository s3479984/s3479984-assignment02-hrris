﻿Option Explicit On
Option Strict On

Public Class popupSearch

    Private Sub popupSearch_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim oController As CustomerController = New CustomerController

        Dim sFn = frmCustomer.txtFirstName.Text
        Dim lsDataFN = oController.findbyFirstName(sFn)

        Dim sCustomerDetails As String
        For Each customer In lsDataFN
            sCustomerDetails = CStr(customer("CustomerID"))
            sCustomerDetails += " | " + CStr(customer("Title"))
            sCustomerDetails += " | " + CStr(customer("Gender"))
            sCustomerDetails += " | " + CStr(customer("FirstName"))
            sCustomerDetails += " | " + CStr(customer("LastName"))
            sCustomerDetails += " | " + CStr(customer("Phone"))
            sCustomerDetails += " | " + CStr(customer("Address"))
            sCustomerDetails += " | " + CStr(customer("Email"))
            sCustomerDetails += " | " + CStr(customer("DOB"))

            lstCustomer.Items.Add(sCustomerDetails)
        Next

    End Sub

End Class