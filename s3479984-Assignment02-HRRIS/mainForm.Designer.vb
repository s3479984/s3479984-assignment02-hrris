﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCustomer = New System.Windows.Forms.Button()
        Me.btnRoom = New System.Windows.Forms.Button()
        Me.btnBooking = New System.Windows.Forms.Button()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NormalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CustomerReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RoomStatusToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RoomByCustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BookingsReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckinDueReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RoomBookingReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BreakToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BookingReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvoicesReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutPageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpPageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCustomer
        '
        Me.btnCustomer.Location = New System.Drawing.Point(56, 110)
        Me.btnCustomer.Name = "btnCustomer"
        Me.btnCustomer.Size = New System.Drawing.Size(75, 23)
        Me.btnCustomer.TabIndex = 0
        Me.btnCustomer.Text = "Custormer"
        Me.btnCustomer.UseVisualStyleBackColor = True
        '
        'btnRoom
        '
        Me.btnRoom.Location = New System.Drawing.Point(214, 110)
        Me.btnRoom.Name = "btnRoom"
        Me.btnRoom.Size = New System.Drawing.Size(75, 23)
        Me.btnRoom.TabIndex = 1
        Me.btnRoom.Text = "Room"
        Me.btnRoom.UseVisualStyleBackColor = True
        '
        'btnBooking
        '
        Me.btnBooking.Location = New System.Drawing.Point(364, 110)
        Me.btnBooking.Name = "btnBooking"
        Me.btnBooking.Size = New System.Drawing.Size(75, 23)
        Me.btnBooking.TabIndex = 1
        Me.btnBooking.Text = "Booking"
        Me.btnBooking.UseVisualStyleBackColor = True
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ReportingToolStripMenuItem
        '
        Me.ReportingToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NormalToolStripMenuItem, Me.BreakToolStripMenuItem})
        Me.ReportingToolStripMenuItem.Name = "ReportingToolStripMenuItem"
        Me.ReportingToolStripMenuItem.Size = New System.Drawing.Size(71, 20)
        Me.ReportingToolStripMenuItem.Text = "Reporting"
        '
        'NormalToolStripMenuItem
        '
        Me.NormalToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CustomerReportToolStripMenuItem, Me.RoomStatusToolStripMenuItem, Me.RoomByCustomerToolStripMenuItem, Me.BookingsReportToolStripMenuItem, Me.CheckinDueReportToolStripMenuItem, Me.RoomBookingReportToolStripMenuItem})
        Me.NormalToolStripMenuItem.Name = "NormalToolStripMenuItem"
        Me.NormalToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
        Me.NormalToolStripMenuItem.Text = "Normal"
        '
        'CustomerReportToolStripMenuItem
        '
        Me.CustomerReportToolStripMenuItem.Name = "CustomerReportToolStripMenuItem"
        Me.CustomerReportToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.CustomerReportToolStripMenuItem.Text = "Customer Report"
        '
        'RoomStatusToolStripMenuItem
        '
        Me.RoomStatusToolStripMenuItem.Name = "RoomStatusToolStripMenuItem"
        Me.RoomStatusToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.RoomStatusToolStripMenuItem.Text = "Room Status"
        '
        'RoomByCustomerToolStripMenuItem
        '
        Me.RoomByCustomerToolStripMenuItem.Name = "RoomByCustomerToolStripMenuItem"
        Me.RoomByCustomerToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.RoomByCustomerToolStripMenuItem.Text = "Room by Customer"
        '
        'BookingsReportToolStripMenuItem
        '
        Me.BookingsReportToolStripMenuItem.Name = "BookingsReportToolStripMenuItem"
        Me.BookingsReportToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.BookingsReportToolStripMenuItem.Text = "Bookings Report"
        '
        'CheckinDueReportToolStripMenuItem
        '
        Me.CheckinDueReportToolStripMenuItem.Name = "CheckinDueReportToolStripMenuItem"
        Me.CheckinDueReportToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.CheckinDueReportToolStripMenuItem.Text = "Checkin Due Report"
        '
        'RoomBookingReportToolStripMenuItem
        '
        Me.RoomBookingReportToolStripMenuItem.Name = "RoomBookingReportToolStripMenuItem"
        Me.RoomBookingReportToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.RoomBookingReportToolStripMenuItem.Text = "Room Booking Report"
        '
        'BreakToolStripMenuItem
        '
        Me.BreakToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BookingReportToolStripMenuItem, Me.InvoicesReportToolStripMenuItem})
        Me.BreakToolStripMenuItem.Name = "BreakToolStripMenuItem"
        Me.BreakToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
        Me.BreakToolStripMenuItem.Text = "Break"
        '
        'BookingReportToolStripMenuItem
        '
        Me.BookingReportToolStripMenuItem.Name = "BookingReportToolStripMenuItem"
        Me.BookingReportToolStripMenuItem.Size = New System.Drawing.Size(156, 22)
        Me.BookingReportToolStripMenuItem.Text = "Booking Report"
        '
        'InvoicesReportToolStripMenuItem
        '
        Me.InvoicesReportToolStripMenuItem.Name = "InvoicesReportToolStripMenuItem"
        Me.InvoicesReportToolStripMenuItem.Size = New System.Drawing.Size(156, 22)
        Me.InvoicesReportToolStripMenuItem.Text = "Invoices Report"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ReportingToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(518, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutPageToolStripMenuItem, Me.HelpPageToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'AboutPageToolStripMenuItem
        '
        Me.AboutPageToolStripMenuItem.Name = "AboutPageToolStripMenuItem"
        Me.AboutPageToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AboutPageToolStripMenuItem.Text = "About Page"
        '
        'HelpPageToolStripMenuItem
        '
        Me.HelpPageToolStripMenuItem.Name = "HelpPageToolStripMenuItem"
        Me.HelpPageToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.HelpPageToolStripMenuItem.Text = "Help Page"
        '
        'mainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(518, 255)
        Me.Controls.Add(Me.btnBooking)
        Me.Controls.Add(Me.btnRoom)
        Me.Controls.Add(Me.btnCustomer)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "mainForm"
        Me.Text = "Main Form"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCustomer As System.Windows.Forms.Button
    Friend WithEvents btnRoom As System.Windows.Forms.Button
    Friend WithEvents btnBooking As System.Windows.Forms.Button
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NormalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustomerReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RoomStatusToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RoomByCustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BookingsReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckinDueReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RoomBookingReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BreakToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BookingReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvoicesReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutPageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpPageToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
