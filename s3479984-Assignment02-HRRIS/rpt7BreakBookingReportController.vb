﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb
Imports System.IO

Public Class rpt7BreakBookingReportController

    Public Const CONNECTION_STRING As String = _
    "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=s3479984-hotelbooking.accdb"

    'Find the record by input ID from the form'
    Public Function findById(sMonth As String, sYear As String) As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'query to execute the sql
            oCommand.CommandText = _
                "SELECT b.[date], b.num_days, b.num_guests, b.checkin_date, b.total_price, b.comments, c.firstname, c.lastname, r.room_number FROM booking b, customer c, room r WHERE MONTH(b.[date]) = " & sMonth & " AND YEAR(b.[date]) = " & sYear & " AND c.customer_id = b.customer_id AND b.room_id = r.room_id ORDER BY b.room_id;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            'parameter you want to get
            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("Date") = CStr(oDataReader("date"))
                htTempData("NumDays") = CStr(oDataReader("num_days"))
                htTempData("NumGuests") = CStr(oDataReader("num_guests"))
                htTempData("CheckinDate") = CStr(oDataReader("checkin_date"))
                htTempData("TotalPrice") = CStr(oDataReader("total_price"))
                htTempData("Comments") = CStr(oDataReader("comments"))
                htTempData("FirstName") = CStr(oDataReader("firstname"))
                htTempData("LastName") = CStr(oDataReader("lastname"))
                htTempData("RoomNumber") = CStr(oDataReader("room_number"))
                lsData.Add(htTempData)
            Loop

            Debug.Print("The record was found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    Public Sub createBreakReport(sMonth As String, sYear As String)
        Debug.Print("CreateReport07 ...")

        Dim lsData = findById(sMonth, sYear)
        'report name go here
        Dim sReportTitle = "Booking Break Report"
        Dim sReportContent = generateBreakReport(lsData, sReportTitle)
        'report file name go here
        Dim sReportFilename = "Report-07.html"
        saveReport(sReportContent, sReportFilename)

        Dim sParam As String = """" & Application.StartupPath & "\" & sReportFilename & """"
        Debug.Print("sParam: " & sParam)
        System.Diagnostics.Process.Start(sParam)

    End Sub

    Private Function generateBreakReport(ByVal lsData As List(Of Hashtable), ByVal sReportTitle As String) As String
        Debug.Print("GenerateBreakReport ...")

        Dim sReportContent As String

        ' 1. Generate the start of the HTML file
        Dim sDoctype As String = "<!DOCTYPE html>"
        Dim sHtmlStartTag As String = "<html lang=""en"">"
        Dim sHeadTitle As String = "<head><title>" & sReportTitle & "</title></head>"
        Dim sBodyStartTag As String = "<body>"
        Dim sReportHeading As String = "</h1>" & sReportTitle & "</h1>"
        sReportContent = sDoctype & vbCrLf & sHtmlStartTag & vbCrLf & sHeadTitle _
         & vbCrLf & sBodyStartTag & vbCrLf & sReportHeading & vbCrLf

        ' 2. Generate the product table and its rows
        Dim sTable = generateControlBreakTable(lsData)
        sReportContent &= sTable & vbCrLf

        ' 3. Generate the end of the HTML file
        Dim sBodyEndTag As String = "</body>"
        Dim sHTMLEndTag As String = "</html>"
        sReportContent &= sBodyEndTag & vbCrLf & sHTMLEndTag

        Return sReportContent

    End Function

    Private Function generateControlBreakTable(ByVal lsData As List(Of Hashtable)) As String

        ' Generate the start of the table
        Dim sTable = "<table border=""1"">" & vbCrLf
        Dim htSample As Hashtable = lsData.Item(0)
        'Dim lsKeys = htSample.Keys
        Dim lsKeys As List(Of String) = New List(Of String)
        lsKeys.Add("Date")
        lsKeys.Add("NumDays")
        lsKeys.Add("NumGuests")
        lsKeys.Add("CheckinDate")
        lsKeys.Add("TotalPrice")
        lsKeys.Add("Comments")
        lsKeys.Add("FistName")
        lsKeys.Add("LastName")
        lsKeys.Add("RoomNumber")

        ' Generate the header row
        Dim sHeaderRow = "<tr>" & vbCrLf
        For Each key In lsKeys
            sHeaderRow &= "<th>" & CStr(key) & "</th>" & vbCrLf
        Next
        sHeaderRow &= "</tr>" & vbCrLf
        Debug.Print("sHeaderRow: " & sHeaderRow)
        sTable &= sHeaderRow

        ' Generate the table rows
        sTable &= generateTableRows(lsData, lsKeys)

        ' Generate the end of the table
        sTable &= "</table>" & vbCrLf

        Return sTable

    End Function

    Private Sub saveReport(ByVal sReportContent As String, ByVal sReportFilename As String)
        Debug.Print("saveReport: " & sReportFilename)
        Dim oReportFile As StreamWriter = New StreamWriter(Application.StartupPath & "\" & sReportFilename)

        ' Check if the file is open before starting to write to it
        If Not (oReportFile Is Nothing) Then
            oReportFile.Write(sReportContent)
            oReportFile.Close()
        End If
    End Sub

    Private Function generateTableRows(ByVal lsData As List(Of Hashtable), _
                                       ByVal lsKeys As List(Of String)) As String

        '1. Initialisation
        Dim sRows As String = ""
        Dim sTableRow As String
        Dim iCountRecordsPerRoom As Integer = 0
        Dim bFirstTime As Boolean = True
        Dim sCurrentControlField As String = ""
        Dim sPreviousControlField As String = ""

        '2. Loop through the list of hashtables
        For Each record In lsData

            '2a. Get a product and set the current key
            Dim booking As Hashtable = record
            sCurrentControlField = CStr(booking("RoomNumber"))

            '2b. Do not check for control break on the first iteration of the loop
            If bFirstTime Then
                bFirstTime = False
            Else
                'Output total row if change in control field
                'And reset the total
                If sCurrentControlField <> sPreviousControlField Then
                    sTableRow = "<tr><td colspan = """ & lsKeys.Count & """>" _
                        & " Total bookings in " & sPreviousControlField _
                        & " is: " & iCountRecordsPerRoom _
                        & "</td></tr>" _
                        & vbCrLf
                    sRows &= sTableRow
                    iCountRecordsPerRoom = 0
                End If
            End If

            ' 2c. Output a normal row for every pass thru' the list
            sTableRow = "<tr>" & vbCrLf
            For Each key In lsKeys
                sTableRow &= "<td>" & CStr(booking(key)) & "</td>" & vbCrLf
            Next
            sTableRow &= "</tr>" & vbCrLf
            Debug.Print("sTableRow: " & sTableRow)
            sRows &= sTableRow

            '2d. Update control field and increment total
            sPreviousControlField = sCurrentControlField
            iCountRecordsPerRoom += 1
        Next

        '3. After the loop, need to output the last total row
        sTableRow = "<tr><td colspan = """ & lsKeys.Count & """>" _
                        & " Total bookings in " & sCurrentControlField _
                        & " is: " & iCountRecordsPerRoom _
                        & "</td></tr>" _
                        & vbCrLf
        sRows &= sTableRow

        Return sRows

    End Function

End Class
