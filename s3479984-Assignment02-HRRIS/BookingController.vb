﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb

Public Class BookingController

    Public Const CONNECTION_STRING As String = _
    "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=s3479984-hotelbooking.accdb"

    'find all record
    Public Function FindAll() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT b.booking_id, b.[date], b.room_id, b.customer_id, b.num_days, b.num_guests, b.checkin_date, b.total_price, b.comments, c.firstname, c.lastname, r.room_number FROM booking b, customer c, room r  WHERE r.room_id = b.room_id AND b.customer_id = c.customer_id;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("BookingID") = CStr(oDataReader("booking_id"))
                htTempData("Date") = CStr(oDataReader("date"))
                htTempData("RoomID") = CStr(oDataReader("room_id"))
                htTempData("CustomerID") = CStr(oDataReader("customer_id"))
                htTempData("NumDays") = CStr(oDataReader("num_days"))
                htTempData("NumGuests") = CStr(oDataReader("num_guests"))
                htTempData("CheckinDate") = CStr(oDataReader("checkin_date"))
                htTempData("TotalPrice") = CStr(oDataReader("total_price"))
                htTempData("Comments") = CStr(oDataReader("comments"))
                htTempData("FirstName") = CStr(oDataReader("firstname"))
                htTempData("LastName") = CStr(oDataReader("lastname"))
                htTempData("RoomNumber") = CStr(oDataReader("room_number"))
                lsData.Add(htTempData)
            Loop

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'find first record
    Public Function firstRecord() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT TOP 1 b.booking_id, b.[date], b.room_id, b.customer_id, b.num_days, b.num_guests, b.checkin_date, b.total_price, b.comments, c.firstname, c.lastname, r.room_number FROM booking b, customer c, room r  WHERE r.room_id = b.room_id AND b.customer_id = c.customer_id;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("BookingID") = CStr(oDataReader("booking_id"))
                htTempData("Date") = CStr(oDataReader("date"))
                htTempData("RoomID") = CStr(oDataReader("room_id"))
                htTempData("CustomerID") = CStr(oDataReader("customer_id"))
                htTempData("NumDays") = CStr(oDataReader("num_days"))
                htTempData("NumGuests") = CStr(oDataReader("num_guests"))
                htTempData("CheckinDate") = CStr(oDataReader("checkin_date"))
                htTempData("TotalPrice") = CStr(oDataReader("total_price"))
                htTempData("Comments") = CStr(oDataReader("comments"))
                htTempData("FirstName") = CStr(oDataReader("firstname"))
                htTempData("LastName") = CStr(oDataReader("lastname"))
                htTempData("RoomNumber") = CStr(oDataReader("room_number"))
                lsData.Add(htTempData)
            Loop

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'insert'
    Public Function insert(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection
            oCommand.CommandText = _
                "INSERT INTO booking ([date], room_id, customer_id, num_days, num_guests, checkin_date, total_price, comments) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"

            oCommand.Parameters.Add("Date", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("RoomID", OleDbType.Integer, 12)
            oCommand.Parameters.Add("CustomerID", OleDbType.Integer, 12)
            oCommand.Parameters.Add("NumberDays", OleDbType.Integer, 12)
            oCommand.Parameters.Add("NumberGuests", OleDbType.Integer, 12)
            oCommand.Parameters.Add("CheckinDate", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("TotalPrice", OleDbType.Integer, 12)
            oCommand.Parameters.Add("Comments", OleDbType.VarChar, 255)

            oCommand.Parameters("Date").Value = CStr(htData("Date"))
            oCommand.Parameters("RoomID").Value = CStr(htData("RoomID"))
            oCommand.Parameters("CustomerID").Value = CStr(htData("CustomerID"))
            oCommand.Parameters("NumberDays").Value = CStr(htData("NumberDays"))
            oCommand.Parameters("NumberGuests").Value = CStr(htData("NumberGuests"))
            oCommand.Parameters("CheckinDate").Value = CStr(htData("CheckinDate"))
            oCommand.Parameters("TotalPrice").Value = CStr(htData("TotalPrice"))
            oCommand.Parameters("Comments").Value = CStr(htData("Comments"))

            oCommand.Prepare()

            iNumRows = oCommand.ExecuteNonQuery()
            Debug.Print(CStr(iNumRows))

            Debug.Print("The record was inserted.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

    'Find the record by input ID from the form'
    Public Function findById(sId As String) As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT b.booking_id, b.[date], b.room_id, b.customer_id, b.num_days, b.num_guests, b.checkin_date, b.total_price, b.comments, c.firstname, c.lastname, r.room_number FROM booking b, customer c, room r   WHERE b.booking_id = ? AND r.room_id = b.room_id AND b.customer_id = c.customer_id;"
            oCommand.Parameters.Add("booking_id", OleDbType.Integer, 8)
            oCommand.Parameters("booking_id").Value = CInt(sId)
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("BookingID") = CStr(oDataReader("booking_id"))
                htTempData("Date") = CStr(oDataReader("date"))
                htTempData("RoomID") = CStr(oDataReader("room_id"))
                htTempData("CustomerID") = CStr(oDataReader("customer_id"))
                htTempData("NumDays") = CStr(oDataReader("num_days"))
                htTempData("NumGuests") = CStr(oDataReader("num_guests"))
                htTempData("CheckinDate") = CStr(oDataReader("checkin_date"))
                htTempData("TotalPrice") = CStr(oDataReader("total_price"))
                htTempData("Comments") = CStr(oDataReader("comments"))
                htTempData("FirstName") = CStr(oDataReader("firstname"))
                htTempData("LastName") = CStr(oDataReader("lastname"))
                htTempData("RoomNumber") = CStr(oDataReader("room_number"))
                lsData.Add(htTempData)
            Loop

            Debug.Print("The record was found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'Update'
    Public Function update(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'TODO
            oCommand.CommandText = _
                "UPDATE booking SET [date] = ?, room_id = ?, customer_id = ?, num_days = ?, num_guests = ?, checkin_date = ?, total_price = ?, comments = ? WHERE booking_id = ?;"

            oCommand.Parameters.Add("Date", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("RoomID", OleDbType.Integer, 12)
            oCommand.Parameters.Add("CustomerID", OleDbType.Integer, 12)
            oCommand.Parameters.Add("NumberDays", OleDbType.Integer, 12)
            oCommand.Parameters.Add("NumberGuests", OleDbType.Integer, 12)
            oCommand.Parameters.Add("CheckinDate", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("TotalPrice", OleDbType.Integer, 12)
            oCommand.Parameters.Add("Comments", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("BookingID", OleDbType.VarChar, 255)

            oCommand.Parameters("Date").Value = CStr(htData("Date"))
            oCommand.Parameters("RoomID").Value = CStr(htData("RoomID"))
            oCommand.Parameters("CustomerID").Value = CStr(htData("CustomerID"))
            oCommand.Parameters("NumberDays").Value = CStr(htData("NumberDays"))
            oCommand.Parameters("NumberGuests").Value = CStr(htData("NumberGuests"))
            oCommand.Parameters("CheckinDate").Value = CStr(htData("CheckinDate"))
            oCommand.Parameters("TotalPrice").Value = CStr(htData("TotalPrice"))
            oCommand.Parameters("Comments").Value = CStr(htData("Comments"))
            oCommand.Parameters("BookingID").Value = CStr(htData("BookingID"))

            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))
            Debug.Print("The record was updated.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record was not updated!")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows
    End Function

    'Delete'
    Public Function delete(sId As String) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'TODO
            oCommand.CommandText = _
               "DELETE FROM booking WHERE booking_id = ?;"
            oCommand.Parameters.Add("booking_id", OleDbType.Integer, 8)
            oCommand.Parameters("booking_id").Value = CInt(sId)
            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))
            Debug.Print("The record was deleted.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record was not deleted!")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

    'find room'
    Public Function findRoom() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT room_id, room_number FROM room;"
            oCommand.Parameters.Add("RoomID", OleDbType.Integer, 15)
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("RoomID") = CStr(oDataReader("room_id"))
                htTempData("RoomNumber") = CStr(oDataReader("room_number"))
                lsData.Add(htTempData)
            Loop

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

End Class