﻿Option Explicit On
Option Strict On

Public Class rpt1CustomerReport

    Dim lsDataCustomer As List(Of Hashtable)
    Public iCustomerID As Integer

    Private Sub frmComboBox_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboCustomerID.DropDownStyle = ComboBoxStyle.DropDownList
        Dim oCustomerController As CustomerController = New CustomerController

        'we already have findCustomer function from CustomerController before
        lsDataCustomer = oCustomerController.findCustomer()
        For Each customer In lsDataCustomer
            cboCustomerID.Items.Add(CStr(customer("CustomerID")))
        Next

    End Sub

    Private Sub cboCustomerID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCustomerID.SelectedIndexChanged

        Dim selectedIndex As Integer = cboCustomerID.SelectedIndex
        Dim selectedItem As Object = cboCustomerID.SelectedItem

        Dim htTempData = lsDataCustomer.Item(selectedIndex)
        iCustomerID = CInt(CStr(htTempData("CustomerID")))

    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnGenerateReport.Click
        Dim oController As rpt1CustomerReportController = New rpt1CustomerReportController
        Dim sId = cboCustomerID.SelectedItem
        oController.createReport(CStr(sId))

    End Sub

End Class