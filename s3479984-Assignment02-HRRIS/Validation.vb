﻿Option Explicit On
Option Strict On

Imports System.Text.RegularExpressions

Public Class Validation

    'validate for numerical
    Public Function isNumericVal(ByVal strVal As String) As Boolean

        Try
            Return IsNumeric(strVal)
        Catch ex As Exception
            Debug.Print("Error: " & ex.Message)

            Return False
        End Try

    End Function

    'validate for alphanumerical 
    Public Function isAlphaNumericVal(ByVal strVal As String) As Boolean

        Dim pattern As Regex = New Regex("[a-zA-Z0-9 _]")

        If strVal.Length > 0 Then
            Return pattern.IsMatch(strVal)
        Else
            Return False
        End If
    End Function

    'validate phone number
    Public Function isPhoneNumber(ByVal strVal As String) As Boolean

        Dim pattern As Regex = New Regex("[0-9]")

        If strVal.Length > 8 And strVal.Length < 11 Then
            Return pattern.IsMatch(strVal)
        Else
            Return False
        End If
    End Function

    'validate email
    Public Function isEmail(ByVal strVal As String) As Boolean

        'get the email regex from the internet
        Dim pattern As Regex = New Regex("^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$")

        If strVal.Length > 0 Then
            Return pattern.IsMatch(strVal)
        Else
            Return False
        End If
    End Function

    'validate date
    'Public Function isDate(ByVal strVal As String) As Boolean

    '    'get the email regex from the internet
    '    Dim pattern As Regex = New Regex("^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$")

    '    If strVal.Length > 0 Then
    '        Return pattern.IsMatch(strVal)
    '    Else
    '        Return False
    '    End If
    'End Function

End Class
