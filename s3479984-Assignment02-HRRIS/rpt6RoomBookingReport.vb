﻿Option Explicit On
Option Strict On

Public Class rpt6RoomBookingReport

    'init public variable for room id
    Dim lsDataRoom As List(Of Hashtable)
    Public iRoomID As Integer

    Private Sub frmComboBox_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboRoomNumber.DropDownStyle = ComboBoxStyle.DropDownList
        cboMonth.DropDownStyle = ComboBoxStyle.DropDownList
        Dim oBookingController As BookingController = New BookingController

        lsDataRoom = oBookingController.findRoom()
        For Each room In lsDataRoom
            cboRoomNumber.Items.Add(CStr(room("RoomNumber")))
        Next

    End Sub

    Private Sub cboRoomNumber_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRoomNumber.SelectedIndexChanged

        Dim selectedIndex As Integer = cboRoomNumber.SelectedIndex
        Dim selectedItem As Object = cboRoomNumber.SelectedItem

        Dim htTempData = lsDataRoom.Item(selectedIndex)
        iRoomID = CInt(CStr(htTempData("RoomID")))

    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnGenerateReport.Click
        Dim oController As rpt6RoomBookingReportController = New rpt6RoomBookingReportController

        Dim sId = iRoomID
        Dim sMonth = cboMonth.SelectedItem
        Dim sYear = txtYear.Text
        oController.createReport(CStr(sId), CStr(sMonth), CStr(sYear))

    End Sub

End Class