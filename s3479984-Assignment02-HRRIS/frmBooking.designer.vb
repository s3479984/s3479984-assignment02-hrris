﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBooking
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBooking))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtpCheckinDate = New System.Windows.Forms.DateTimePicker()
        Me.cboCustomerName = New System.Windows.Forms.ComboBox()
        Me.dtpDate = New System.Windows.Forms.DateTimePicker()
        Me.cboRoomNumber = New System.Windows.Forms.ComboBox()
        Me.picErrorComments = New System.Windows.Forms.PictureBox()
        Me.txtComments = New System.Windows.Forms.TextBox()
        Me.lblHeadingComment = New System.Windows.Forms.Label()
        Me.picErrorTotalPrice = New System.Windows.Forms.PictureBox()
        Me.txtTotalPrice = New System.Windows.Forms.TextBox()
        Me.lblHeadinTotalPrice = New System.Windows.Forms.Label()
        Me.picErrorCheckingDate = New System.Windows.Forms.PictureBox()
        Me.picErrorNumberGuests = New System.Windows.Forms.PictureBox()
        Me.picErrorNumberDays = New System.Windows.Forms.PictureBox()
        Me.picErrorDate = New System.Windows.Forms.PictureBox()
        Me.txtNumberGuests = New System.Windows.Forms.TextBox()
        Me.txtNumberDays = New System.Windows.Forms.TextBox()
        Me.lblHeadingNumberDays = New System.Windows.Forms.Label()
        Me.lblHeadingCheckinDate = New System.Windows.Forms.Label()
        Me.lblHeadingNumberGuests = New System.Windows.Forms.Label()
        Me.lblHeadingCustomerIName = New System.Windows.Forms.Label()
        Me.lblHeadingRoomName = New System.Windows.Forms.Label()
        Me.lblHeadingDate = New System.Windows.Forms.Label()
        Me.txtBookingID = New System.Windows.Forms.TextBox()
        Me.lblHeadingBookingID = New System.Windows.Forms.Label()
        Me.btnInsert = New System.Windows.Forms.Button()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnLast = New System.Windows.Forms.Button()
        Me.btnFirst = New System.Windows.Forms.Button()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.picErrorComments, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorTotalPrice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorCheckingDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorNumberGuests, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorNumberDays, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorDate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpCheckinDate)
        Me.GroupBox1.Controls.Add(Me.cboCustomerName)
        Me.GroupBox1.Controls.Add(Me.dtpDate)
        Me.GroupBox1.Controls.Add(Me.cboRoomNumber)
        Me.GroupBox1.Controls.Add(Me.picErrorComments)
        Me.GroupBox1.Controls.Add(Me.txtComments)
        Me.GroupBox1.Controls.Add(Me.lblHeadingComment)
        Me.GroupBox1.Controls.Add(Me.picErrorTotalPrice)
        Me.GroupBox1.Controls.Add(Me.txtTotalPrice)
        Me.GroupBox1.Controls.Add(Me.lblHeadinTotalPrice)
        Me.GroupBox1.Controls.Add(Me.picErrorCheckingDate)
        Me.GroupBox1.Controls.Add(Me.picErrorNumberGuests)
        Me.GroupBox1.Controls.Add(Me.picErrorNumberDays)
        Me.GroupBox1.Controls.Add(Me.picErrorDate)
        Me.GroupBox1.Controls.Add(Me.txtNumberGuests)
        Me.GroupBox1.Controls.Add(Me.txtNumberDays)
        Me.GroupBox1.Controls.Add(Me.lblHeadingNumberDays)
        Me.GroupBox1.Controls.Add(Me.lblHeadingCheckinDate)
        Me.GroupBox1.Controls.Add(Me.lblHeadingNumberGuests)
        Me.GroupBox1.Controls.Add(Me.lblHeadingCustomerIName)
        Me.GroupBox1.Controls.Add(Me.lblHeadingRoomName)
        Me.GroupBox1.Controls.Add(Me.lblHeadingDate)
        Me.GroupBox1.Controls.Add(Me.txtBookingID)
        Me.GroupBox1.Controls.Add(Me.lblHeadingBookingID)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(380, 283)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Booking Details"
        '
        'dtpCheckinDate
        '
        Me.dtpCheckinDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCheckinDate.Location = New System.Drawing.Point(146, 195)
        Me.dtpCheckinDate.Name = "dtpCheckinDate"
        Me.dtpCheckinDate.Size = New System.Drawing.Size(200, 20)
        Me.dtpCheckinDate.TabIndex = 15
        '
        'cboCustomerName
        '
        Me.cboCustomerName.FormattingEnabled = True
        Me.cboCustomerName.Location = New System.Drawing.Point(146, 104)
        Me.cboCustomerName.Name = "cboCustomerName"
        Me.cboCustomerName.Size = New System.Drawing.Size(202, 21)
        Me.cboCustomerName.TabIndex = 29
        '
        'dtpDate
        '
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(148, 45)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(200, 20)
        Me.dtpDate.TabIndex = 14
        '
        'cboRoomNumber
        '
        Me.cboRoomNumber.FormattingEnabled = True
        Me.cboRoomNumber.Location = New System.Drawing.Point(146, 74)
        Me.cboRoomNumber.Name = "cboRoomNumber"
        Me.cboRoomNumber.Size = New System.Drawing.Size(202, 21)
        Me.cboRoomNumber.TabIndex = 28
        '
        'picErrorComments
        '
        Me.picErrorComments.ErrorImage = Nothing
        Me.picErrorComments.Image = CType(resources.GetObject("picErrorComments.Image"), System.Drawing.Image)
        Me.picErrorComments.Location = New System.Drawing.Point(354, 247)
        Me.picErrorComments.Name = "picErrorComments"
        Me.picErrorComments.Size = New System.Drawing.Size(25, 20)
        Me.picErrorComments.TabIndex = 27
        Me.picErrorComments.TabStop = False
        Me.picErrorComments.Visible = False
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(146, 247)
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Size = New System.Drawing.Size(202, 20)
        Me.txtComments.TabIndex = 26
        '
        'lblHeadingComment
        '
        Me.lblHeadingComment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingComment.Location = New System.Drawing.Point(6, 246)
        Me.lblHeadingComment.Name = "lblHeadingComment"
        Me.lblHeadingComment.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingComment.TabIndex = 25
        Me.lblHeadingComment.Text = "Comments"
        Me.lblHeadingComment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'picErrorTotalPrice
        '
        Me.picErrorTotalPrice.ErrorImage = Nothing
        Me.picErrorTotalPrice.Image = CType(resources.GetObject("picErrorTotalPrice.Image"), System.Drawing.Image)
        Me.picErrorTotalPrice.Location = New System.Drawing.Point(354, 221)
        Me.picErrorTotalPrice.Name = "picErrorTotalPrice"
        Me.picErrorTotalPrice.Size = New System.Drawing.Size(25, 20)
        Me.picErrorTotalPrice.TabIndex = 24
        Me.picErrorTotalPrice.TabStop = False
        Me.picErrorTotalPrice.Visible = False
        '
        'txtTotalPrice
        '
        Me.txtTotalPrice.Location = New System.Drawing.Point(146, 221)
        Me.txtTotalPrice.Name = "txtTotalPrice"
        Me.txtTotalPrice.Size = New System.Drawing.Size(202, 20)
        Me.txtTotalPrice.TabIndex = 23
        '
        'lblHeadinTotalPrice
        '
        Me.lblHeadinTotalPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadinTotalPrice.Location = New System.Drawing.Point(6, 220)
        Me.lblHeadinTotalPrice.Name = "lblHeadinTotalPrice"
        Me.lblHeadinTotalPrice.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadinTotalPrice.TabIndex = 22
        Me.lblHeadinTotalPrice.Text = "Total Price"
        Me.lblHeadinTotalPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'picErrorCheckingDate
        '
        Me.picErrorCheckingDate.ErrorImage = Nothing
        Me.picErrorCheckingDate.Image = CType(resources.GetObject("picErrorCheckingDate.Image"), System.Drawing.Image)
        Me.picErrorCheckingDate.Location = New System.Drawing.Point(354, 195)
        Me.picErrorCheckingDate.Name = "picErrorCheckingDate"
        Me.picErrorCheckingDate.Size = New System.Drawing.Size(25, 20)
        Me.picErrorCheckingDate.TabIndex = 21
        Me.picErrorCheckingDate.TabStop = False
        Me.picErrorCheckingDate.Visible = False
        '
        'picErrorNumberGuests
        '
        Me.picErrorNumberGuests.ErrorImage = Nothing
        Me.picErrorNumberGuests.Image = CType(resources.GetObject("picErrorNumberGuests.Image"), System.Drawing.Image)
        Me.picErrorNumberGuests.Location = New System.Drawing.Point(354, 165)
        Me.picErrorNumberGuests.Name = "picErrorNumberGuests"
        Me.picErrorNumberGuests.Size = New System.Drawing.Size(25, 20)
        Me.picErrorNumberGuests.TabIndex = 20
        Me.picErrorNumberGuests.TabStop = False
        Me.picErrorNumberGuests.Visible = False
        '
        'picErrorNumberDays
        '
        Me.picErrorNumberDays.ErrorImage = Nothing
        Me.picErrorNumberDays.Image = CType(resources.GetObject("picErrorNumberDays.Image"), System.Drawing.Image)
        Me.picErrorNumberDays.Location = New System.Drawing.Point(354, 133)
        Me.picErrorNumberDays.Name = "picErrorNumberDays"
        Me.picErrorNumberDays.Size = New System.Drawing.Size(25, 20)
        Me.picErrorNumberDays.TabIndex = 19
        Me.picErrorNumberDays.TabStop = False
        Me.picErrorNumberDays.Visible = False
        '
        'picErrorDate
        '
        Me.picErrorDate.ErrorImage = Nothing
        Me.picErrorDate.Image = CType(resources.GetObject("picErrorDate.Image"), System.Drawing.Image)
        Me.picErrorDate.Location = New System.Drawing.Point(354, 45)
        Me.picErrorDate.Name = "picErrorDate"
        Me.picErrorDate.Size = New System.Drawing.Size(25, 20)
        Me.picErrorDate.TabIndex = 16
        Me.picErrorDate.TabStop = False
        Me.picErrorDate.Visible = False
        '
        'txtNumberGuests
        '
        Me.txtNumberGuests.Location = New System.Drawing.Point(146, 165)
        Me.txtNumberGuests.Name = "txtNumberGuests"
        Me.txtNumberGuests.Size = New System.Drawing.Size(202, 20)
        Me.txtNumberGuests.TabIndex = 13
        '
        'txtNumberDays
        '
        Me.txtNumberDays.Location = New System.Drawing.Point(146, 133)
        Me.txtNumberDays.Name = "txtNumberDays"
        Me.txtNumberDays.Size = New System.Drawing.Size(202, 20)
        Me.txtNumberDays.TabIndex = 12
        '
        'lblHeadingNumberDays
        '
        Me.lblHeadingNumberDays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingNumberDays.Location = New System.Drawing.Point(6, 132)
        Me.lblHeadingNumberDays.Name = "lblHeadingNumberDays"
        Me.lblHeadingNumberDays.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingNumberDays.TabIndex = 9
        Me.lblHeadingNumberDays.Text = "Number Days"
        Me.lblHeadingNumberDays.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingCheckinDate
        '
        Me.lblHeadingCheckinDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingCheckinDate.Location = New System.Drawing.Point(6, 194)
        Me.lblHeadingCheckinDate.Name = "lblHeadingCheckinDate"
        Me.lblHeadingCheckinDate.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingCheckinDate.TabIndex = 7
        Me.lblHeadingCheckinDate.Text = "Checking Date"
        Me.lblHeadingCheckinDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingNumberGuests
        '
        Me.lblHeadingNumberGuests.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingNumberGuests.Location = New System.Drawing.Point(6, 164)
        Me.lblHeadingNumberGuests.Name = "lblHeadingNumberGuests"
        Me.lblHeadingNumberGuests.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingNumberGuests.TabIndex = 6
        Me.lblHeadingNumberGuests.Text = "Number Guests"
        Me.lblHeadingNumberGuests.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingCustomerIName
        '
        Me.lblHeadingCustomerIName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingCustomerIName.Location = New System.Drawing.Point(6, 105)
        Me.lblHeadingCustomerIName.Name = "lblHeadingCustomerIName"
        Me.lblHeadingCustomerIName.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingCustomerIName.TabIndex = 5
        Me.lblHeadingCustomerIName.Text = "Customer Name"
        Me.lblHeadingCustomerIName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingRoomName
        '
        Me.lblHeadingRoomName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingRoomName.Location = New System.Drawing.Point(6, 75)
        Me.lblHeadingRoomName.Name = "lblHeadingRoomName"
        Me.lblHeadingRoomName.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingRoomName.TabIndex = 4
        Me.lblHeadingRoomName.Text = "Room Number"
        Me.lblHeadingRoomName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingDate
        '
        Me.lblHeadingDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingDate.Location = New System.Drawing.Point(6, 45)
        Me.lblHeadingDate.Name = "lblHeadingDate"
        Me.lblHeadingDate.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingDate.TabIndex = 2
        Me.lblHeadingDate.Text = "Date"
        Me.lblHeadingDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtBookingID
        '
        Me.txtBookingID.Location = New System.Drawing.Point(146, 19)
        Me.txtBookingID.Name = "txtBookingID"
        Me.txtBookingID.Size = New System.Drawing.Size(202, 20)
        Me.txtBookingID.TabIndex = 1
        '
        'lblHeadingBookingID
        '
        Me.lblHeadingBookingID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingBookingID.Location = New System.Drawing.Point(6, 16)
        Me.lblHeadingBookingID.Name = "lblHeadingBookingID"
        Me.lblHeadingBookingID.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingBookingID.TabIndex = 1
        Me.lblHeadingBookingID.Text = "Booking ID"
        Me.lblHeadingBookingID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnInsert
        '
        Me.btnInsert.Location = New System.Drawing.Point(398, 31)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(215, 20)
        Me.btnInsert.TabIndex = 1
        Me.btnInsert.Text = "Insert"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'btnFind
        '
        Me.btnFind.Location = New System.Drawing.Point(398, 58)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(215, 20)
        Me.btnFind.TabIndex = 7
        Me.btnFind.Text = "Find"
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(398, 114)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(215, 20)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(398, 88)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(215, 20)
        Me.btnUpdate.TabIndex = 5
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(398, 176)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(215, 20)
        Me.btnClear.TabIndex = 8
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnLast
        '
        Me.btnLast.Location = New System.Drawing.Point(547, 145)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(30, 23)
        Me.btnLast.TabIndex = 13
        Me.btnLast.Text = ">|"
        Me.btnLast.UseVisualStyleBackColor = True
        '
        'btnFirst
        '
        Me.btnFirst.Location = New System.Drawing.Point(439, 145)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(30, 23)
        Me.btnFirst.TabIndex = 12
        Me.btnFirst.Text = "|<"
        Me.btnFirst.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.Location = New System.Drawing.Point(475, 145)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(30, 23)
        Me.btnPrev.TabIndex = 11
        Me.btnPrev.Text = "<"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(511, 145)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(30, 23)
        Me.btnNext.TabIndex = 10
        Me.btnNext.Text = ">"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'frmBooking
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(624, 317)
        Me.Controls.Add(Me.btnLast)
        Me.Controls.Add(Me.btnFirst)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmBooking"
        Me.Text = "booking"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.picErrorComments, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorTotalPrice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorCheckingDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorNumberGuests, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorNumberDays, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorDate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNumberGuests As System.Windows.Forms.TextBox
    Friend WithEvents txtNumberDays As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadingNumberDays As System.Windows.Forms.Label
    Friend WithEvents lblHeadingCheckinDate As System.Windows.Forms.Label
    Friend WithEvents lblHeadingNumberGuests As System.Windows.Forms.Label
    Friend WithEvents lblHeadingCustomerIName As System.Windows.Forms.Label
    Friend WithEvents lblHeadingRoomName As System.Windows.Forms.Label
    Friend WithEvents lblHeadingDate As System.Windows.Forms.Label
    Friend WithEvents txtBookingID As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadingBookingID As System.Windows.Forms.Label
    Friend WithEvents btnInsert As System.Windows.Forms.Button
    Friend WithEvents picErrorCheckingDate As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorNumberGuests As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorNumberDays As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorDate As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorTotalPrice As System.Windows.Forms.PictureBox
    Friend WithEvents txtTotalPrice As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadinTotalPrice As System.Windows.Forms.Label
    Friend WithEvents picErrorComments As System.Windows.Forms.PictureBox
    Friend WithEvents txtComments As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadingComment As System.Windows.Forms.Label
    Friend WithEvents cboRoomNumber As System.Windows.Forms.ComboBox
    Friend WithEvents cboCustomerName As System.Windows.Forms.ComboBox
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnLast As System.Windows.Forms.Button
    Friend WithEvents btnFirst As System.Windows.Forms.Button
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpCheckinDate As System.Windows.Forms.DateTimePicker
End Class
