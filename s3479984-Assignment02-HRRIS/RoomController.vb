﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb

Public Class RoomController

    Public Const CONNECTION_STRING As String = _
    "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=s3479984-hotelbooking.accdb"

    'find all record
    Public Function findAll() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT * FROM room;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("RoomID") = CStr(oDataReader("room_id"))
                htTempData("RoomNumber") = CStr(oDataReader("room_number"))
                htTempData("Type") = CStr(oDataReader("type"))
                htTempData("Price") = CStr(oDataReader("price"))
                htTempData("NumBeds") = CStr(oDataReader("num_beds"))
                htTempData("Availability") = CStr(oDataReader("availability"))
                htTempData("Floor") = CStr(oDataReader("floor"))
                htTempData("Description") = CStr(oDataReader("description"))
                lsData.Add(htTempData)
            Loop

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'find first record
    Public Function firstRecord() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT TOP 1 * FROM room;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("RoomID") = CStr(oDataReader("room_id"))
                htTempData("RoomNumber") = CStr(oDataReader("room_number"))
                htTempData("Type") = CStr(oDataReader("type"))
                htTempData("Price") = CStr(oDataReader("price"))
                htTempData("NumBeds") = CStr(oDataReader("num_beds"))
                htTempData("Availability") = CStr(oDataReader("availability"))
                htTempData("Floor") = CStr(oDataReader("floor"))
                htTempData("Description") = CStr(oDataReader("description"))
                lsData.Add(htTempData)
            Loop

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'insert'
    Public Function insert(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection
            oCommand.CommandText = _
                "INSERT INTO room (room_number, type, price, num_beds, availability, floor, description) VALUES (?, ?, ?, ?, ?, ?, ?);"

            oCommand.Parameters.Add("RoomNumber", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Type", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Price", OleDbType.Integer, 12)
            oCommand.Parameters.Add("NumBeds", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Availability", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Floor", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Description", OleDbType.VarChar, 255)

            oCommand.Parameters("RoomNumber").Value = CStr(htData("RoomNumber"))
            oCommand.Parameters("Type").Value = CStr(htData("Type"))
            oCommand.Parameters("Price").Value = CInt(htData("Price"))
            oCommand.Parameters("NumBeds").Value = CStr(htData("NumBeds"))
            oCommand.Parameters("Availability").Value = CStr(htData("Availability"))
            oCommand.Parameters("Floor").Value = CStr(htData("Floor"))
            oCommand.Parameters("Description").Value = CStr(htData("Description"))

            oCommand.Prepare()

            iNumRows = oCommand.ExecuteNonQuery()
            Debug.Print(CStr(iNumRows))

            Debug.Print("The record was inserted.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

    'Find the record by input ID from the form'
    Public Function findById(sId As String) As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT * FROM room  WHERE room_id = ?;"
            oCommand.Parameters.Add("room_id", OleDbType.Integer, 8)
            oCommand.Parameters("room_id").Value = CInt(sId)
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("RoomID") = CStr(oDataReader("room_id"))
                htTempData("RoomNumber") = CStr(oDataReader("room_number"))
                htTempData("Type") = CStr(oDataReader("type"))
                htTempData("Price") = CStr(oDataReader("price"))
                htTempData("NumBeds") = CStr(oDataReader("num_beds"))
                htTempData("Availability") = CStr(oDataReader("availability"))
                htTempData("Floor") = CStr(oDataReader("floor"))
                htTempData("Description") = CStr(oDataReader("description"))
                lsData.Add(htTempData)
            Loop

            Debug.Print("The record was found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'Update'
    Public Function update(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'TODO
            oCommand.CommandText = _
                "UPDATE room  SET room_number = ?, type = ?, price = ?, num_beds = ?, availability = ?, floor = ?, description = ? WHERE room_id = ?;"

            oCommand.Parameters.Add("RoomNumber", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Type", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Price", OleDbType.Integer, 12)
            oCommand.Parameters.Add("NumBeds", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Availability", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Floor", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Description", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("RoomID", OleDbType.VarChar, 255)

            oCommand.Parameters("RoomNumber").Value = CStr(htData("RoomNumber"))
            oCommand.Parameters("Type").Value = CStr(htData("Type"))
            oCommand.Parameters("Price").Value = CInt(htData("Price"))
            oCommand.Parameters("NumBeds").Value = CStr(htData("NumBeds"))
            oCommand.Parameters("Availability").Value = CStr(htData("Availability"))
            oCommand.Parameters("Floor").Value = CStr(htData("Floor"))
            oCommand.Parameters("Description").Value = CStr(htData("Description"))
            oCommand.Parameters("RoomID").Value = CStr(htData("RoomID"))

            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))
            Debug.Print("The record was updated.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record was not updated!")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows
    End Function

    'Delete'
    Public Function delete(sId As String) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'TODO
            oCommand.CommandText = _
               "DELETE FROM room WHERE room_id = ?;"
            oCommand.Parameters.Add("room_id", OleDbType.Integer, 8)
            oCommand.Parameters("room_id").Value = CInt(sId)
            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))
            Debug.Print("The record was deleted.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record was not deleted!")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

End Class
