﻿Option Explicit On
Option Strict On

Imports System.Drawing

Public Class frmBooking

    'init public variable for room id
    Dim lsDataRoom As List(Of Hashtable)
    Public iRoomID As Integer

    'init public variable for customer id
    Dim lsDataCustomer As List(Of Hashtable)
    Public iCustomerID As Integer

    'our initial value for our minimun value to be used later for navigation
    Public minval As Integer = 0
    'the integer thata will hold the maximum value
    Public maxval As Integer
    'init lsNavData as all data for navigation
    Public lsNavData As New List(Of Hashtable)

    Private Sub frmComboBox_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboRoomNumber.DropDownStyle = ComboBoxStyle.DropDownList
        Dim oBookingController As BookingController = New BookingController
        cboCustomerName.DropDownStyle = ComboBoxStyle.DropDownList
        Dim oCustomerController As CustomerController = New CustomerController

        lsDataRoom = oBookingController.findRoom()
        For Each room In lsDataRoom
            cboRoomNumber.Items.Add(CStr(room("RoomNumber")))
        Next

        lsDataCustomer = oCustomerController.findCustomer()
        For Each customer In lsDataCustomer
            cboCustomerName.Items.Add(CStr(customer("FirstName")) + " " + CStr(customer("LastName")))
        Next

        'find first record
        Dim lsData = oBookingController.firstRecord()

        If lsData.Count = 1 Then
            populateFormFields(lsData.Item(0))
        Else
            'do nothing
        End If

        'find all record for navigation
        Dim lsNavData = oBookingController.FindAll()
        maxval = lsNavData.Count()

    End Sub

    Private Sub cboRoomNumber_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRoomNumber.SelectedIndexChanged

        Dim selectedIndex As Integer = cboRoomNumber.SelectedIndex
        Dim selectedItem As Object = cboRoomNumber.SelectedItem

        Dim htTempData = lsDataRoom.Item(selectedIndex)
        iRoomID = CInt(CStr(htTempData("RoomID")))

    End Sub

    Private Sub cboCustomerName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCustomerName.SelectedIndexChanged

        Dim selectedIndex As Integer = cboCustomerName.SelectedIndex
        Dim selectedItem As Object = cboCustomerName.SelectedItem

        Dim htTempData = lsDataCustomer.Item(selectedIndex)
        iCustomerID = CInt(CStr(htTempData("CustomerID")))

    End Sub

    'button insert'
    Private Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click

        Dim bIsValid = validateFormData()

        If bIsValid Then
            Dim htData As Hashtable = New Hashtable
            htData("Date") = dtpDate.Text
            htData("RoomID") = iRoomID
            htData("CustomerID") = iCustomerID
            htData("NumberDays") = txtNumberDays.Text
            htData("NumberGuests") = txtNumberGuests.Text
            htData("CheckinDate") = dtpCheckinDate.Text
            htData("TotalPrice") = txtTotalPrice.Text
            htData("Comments") = txtComments.Text

            Dim oBookingController As BookingController = New BookingController
            oBookingController.insert(htData)

        Else
            ' Do nothing in this case
        End If

    End Sub

    'validate the form'
    Private Function validateFormData() As Boolean

        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim tt As New ToolTip()

        'validate date
        'bIsValid = oValidation.isDate(dtpDate.Text)
        'If bIsValid Then
        '    picErrorDate.Visible = False
        'Else
        '    picErrorDate.Visible = True
        '    tt.SetToolTip(picErrorDate, "Value is not correct")
        '    bAllFieldsValid = False
        'End If

        'validate number days
        bIsValid = IsNumeric(txtNumberDays.Text)
        If bIsValid Then
            picErrorNumberDays.Visible = False
        Else
            picErrorNumberDays.Visible = True
            tt.SetToolTip(picErrorNumberDays, "Value is not numeric")
            bAllFieldsValid = False
        End If

        'validate number guests
        bIsValid = IsNumeric(txtNumberGuests.Text)
        If bIsValid Then
            picErrorNumberGuests.Visible = False
        Else
            picErrorNumberGuests.Visible = True
            tt.SetToolTip(picErrorNumberGuests, "Value is not alphanumeric")
            bAllFieldsValid = False
        End If

        'validate checking date
        'bIsValid = oValidation.isDate(dtpCheckinDate.Text)
        'If bIsValid Then
        '    picErrorCheckingDate.Visible = False
        'Else
        '    picErrorCheckingDate.Visible = True
        '    tt.SetToolTip(picErrorCheckingDate, "Value is not correct")
        '    bAllFieldsValid = False
        'End If

        'validate total price
        bIsValid = IsNumeric(txtTotalPrice.Text)
        If bIsValid Then
            picErrorTotalPrice.Visible = False
        Else
            picErrorTotalPrice.Visible = True
            tt.SetToolTip(picErrorTotalPrice, "Value is not numeric")
            bAllFieldsValid = False
        End If

        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        Else
            MsgBox("One of the fields was invalid")
        End If

        Return bAllFieldsValid

    End Function

    'button find
    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click

        Dim oController As BookingController = New BookingController

        Dim sId = txtBookingID.Text
        Dim lsData = oController.findById(sId)

        If lsData.Count = 1 Then
            populateFormFields(lsData.Item(0))
        Else
            Debug.Print("No records were found")
        End If

    End Sub

    'populate data for button find
    Private Sub populateFormFields(htdata As Hashtable)

        txtBookingID.Text = CStr(htdata("BookingID"))
        dtpDate.Text = CStr(htdata("Date"))

        'these 2 are tricky you need to join query to just display the item (workaround)
        cboRoomNumber.SelectedItem = CStr(htdata("RoomNumber"))
        cboCustomerName.SelectedItem = (CStr(htdata("FirstName")) & " " & CStr(htdata("LastName")))

        txtNumberDays.Text = CStr(htdata("NumDays"))
        txtNumberGuests.Text = CStr(htdata("NumGuests"))
        dtpCheckinDate.Text = CStr(htdata("CheckinDate"))
        txtTotalPrice.Text = CStr(htdata("TotalPrice"))
        txtComments.Text = CStr(htdata("Comments"))

    End Sub

    'button update'
    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim oController As BookingController = New BookingController

        Dim bIsValid = validateFormData()

        If bIsValid Then
            Dim htData As Hashtable = New Hashtable
            htData("Date") = dtpDate.Text
            htData("RoomID") = iRoomID
            htData("CustomerID") = iCustomerID
            htData("NumberDays") = txtNumberDays.Text
            htData("NumberGuests") = txtNumberGuests.Text
            htData("CheckinDate") = dtpCheckinDate.Text
            htData("TotalPrice") = txtTotalPrice.Text
            htData("Comments") = txtComments.Text
            htData("BookingID") = txtBookingID.Text

            Dim iNumRows = oController.update(htData)

            If iNumRows = 1 Then
                Debug.Print("The record was updated. Use the find button to check ...")
            Else
                Debug.Print("The record was not updated!")
            End If

        Else
            'nothing to do here
        End If

    End Sub

    'button delete'
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim oController As BookingController = New BookingController
        Dim sId = txtBookingID.Text
        Dim iNumRows = oController.delete(sId)

        If MsgBox("Are you sure?", MsgBoxStyle.YesNoCancel, "Title") = MsgBoxResult.Yes Then
            If iNumRows = 1 Then
                clearForm()
                Debug.Print("The record was deleted. Use the find button to check ...")
            Else
                Debug.Print("The record was not deleted!")
            End If
        End If

    End Sub

    'clear everything on the form
    Private Sub clearForm()
        txtBookingID.Clear()
        cboRoomNumber.Items.Clear()
        cboCustomerName.Items.Clear()
        txtNumberDays.Clear()
        txtNumberGuests.Clear()
        txtTotalPrice.Clear()
        txtComments.Clear()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        clearForm()
    End Sub

    'next record
    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        Dim oController As BookingController = New BookingController
        Dim lsNavData = oController.findAll()

        If minval <> maxval - 1 Then
            'minval is incremented by 1
            minval = minval + 1
            'throw the value of minval to nav procedure
            populateFormFields(lsNavData.Item(minval))

        Else
            MsgBox("Last record!")
        End If

    End Sub

    'prev record
    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrev.Click

        Dim oController As BookingController = New BookingController
        Dim lsNavData = oController.findAll()

        'check if minval is still greater than zero
        If minval > 0 Then
            'minval will be decremented by one
            minval = minval - 1
            populateFormFields(lsNavData.Item(minval))

        ElseIf minval = -1 Then
            MsgBox("No results found!")

        ElseIf minval = 0 Then
            MsgBox("First Record")
            populateFormFields(lsNavData.Item(minval))
        End If

    End Sub

    'first record
    Private Sub btnFirst_Click(sender As Object, e As EventArgs) Handles btnFirst.Click
        Dim oController As BookingController = New BookingController
        Dim lsNavData = oController.findAll()

        If minval <> 0 Then
            minval = 0
            populateFormFields(lsNavData.Item(minval))
        End If

    End Sub

    'last record
    Private Sub btnLast_Click(sender As Object, e As EventArgs) Handles btnLast.Click

        Dim oController As BookingController = New BookingController
        Dim lsNavData = oController.findAll()

        If minval <> maxval Then
            minval = maxval - 1
            populateFormFields(lsNavData.Item(minval))
        End If

    End Sub
End Class
