﻿Public Class mainForm

    Private Sub btnCustomer_Click(sender As Object, e As EventArgs) Handles btnCustomer.Click
        frmCustomer.Show()
    End Sub

    Private Sub btnRoom_Click(sender As Object, e As EventArgs) Handles btnRoom.Click
        frmRoom.Show()
    End Sub

    Private Sub btnBooking_Click(sender As Object, e As EventArgs) Handles btnBooking.Click
        frmBooking.Show()
    End Sub

    Private Sub CustomerReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CustomerReportToolStripMenuItem.Click
        rpt1CustomerReport.Show()
    End Sub

    Private Sub RoomStatusToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RoomStatusToolStripMenuItem.Click
        rpt2RoomStatusReport.Show()
    End Sub

    Private Sub RoomByCustomerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RoomByCustomerToolStripMenuItem.Click
        rpt3RoombyCustomerReport.Show()
    End Sub

    Private Sub BookingsReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BookingsReportToolStripMenuItem.Click
        rpt4BookingReport.Show()
    End Sub

    Private Sub CheckinDueReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CheckinDueReportToolStripMenuItem.Click
        rpt5CheckinDueReport.Show()
    End Sub

    Private Sub RoomBookingReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RoomBookingReportToolStripMenuItem.Click
        rpt6RoomBookingReport.Show()
    End Sub

    Private Sub BookingReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BookingReportToolStripMenuItem.Click
        rpt7BreakBookingReport.Show()
    End Sub

    Private Sub InvoicesReportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InvoicesReportToolStripMenuItem.Click
        rpt8breakInvoicesReport.Show()
    End Sub

    Private Sub AboutPageToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutPageToolStripMenuItem.Click
        Process.Start("pages\index.html")
    End Sub

    Private Sub HelpPageToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HelpPageToolStripMenuItem.Click
        Process.Start("pages\help.html")
    End Sub
End Class