﻿Option Explicit On
Option Strict On

Public Class rpt3RoombyCustomerReport

    'init public variable for customer id
    Dim lsDataCustomer As List(Of Hashtable)
    Public iCustomerID As Integer

    Private Sub frmComboBox_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'change 2 cbo to dropdownlist
        cboCustomerName.DropDownStyle = ComboBoxStyle.DropDownList
        cboMonth.DropDownStyle = ComboBoxStyle.DropDownList

        Dim oCustomerController As CustomerController = New CustomerController

        lsDataCustomer = oCustomerController.findCustomer()
        For Each customer In lsDataCustomer
            cboCustomerName.Items.Add(CStr(customer("FirstName")) + " " + CStr(customer("LastName")))
        Next

    End Sub

    Private Sub cboCustomerName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCustomerName.SelectedIndexChanged

        Dim selectedIndex As Integer = cboCustomerName.SelectedIndex
        Dim selectedItem As Object = cboCustomerName.SelectedItem

        Dim htTempData = lsDataCustomer.Item(selectedIndex)
        iCustomerID = CInt(CStr(htTempData("CustomerID")))

    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnGenerateReport.Click
        Dim oController As rpt3RoombyCustomerReportController = New rpt3RoombyCustomerReportController

        Dim sId = iCustomerID
        Dim sMonth = cboMonth.SelectedItem
        Dim sYear = txtYear.Text
        oController.createReport(CStr(sId), CStr(sMonth), CStr(sYear))

    End Sub

End Class