﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCustomer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCustomer))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtpDOB = New System.Windows.Forms.DateTimePicker()
        Me.cbGender = New System.Windows.Forms.ComboBox()
        Me.picErrorCustomerId = New System.Windows.Forms.PictureBox()
        Me.picErrorDOB = New System.Windows.Forms.PictureBox()
        Me.lblHeadingDOB = New System.Windows.Forms.Label()
        Me.picErrorEmail = New System.Windows.Forms.PictureBox()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.lblHeadingEmail = New System.Windows.Forms.Label()
        Me.picErrorAddress = New System.Windows.Forms.PictureBox()
        Me.picErrorPhone = New System.Windows.Forms.PictureBox()
        Me.picErrorLastName = New System.Windows.Forms.PictureBox()
        Me.picErrorFirstName = New System.Windows.Forms.PictureBox()
        Me.picErrorGender = New System.Windows.Forms.PictureBox()
        Me.picErrorTitle = New System.Windows.Forms.PictureBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.txtPhone = New System.Windows.Forms.TextBox()
        Me.txtLastName = New System.Windows.Forms.TextBox()
        Me.txtFirstName = New System.Windows.Forms.TextBox()
        Me.lblHeadingLastName = New System.Windows.Forms.Label()
        Me.lblHeadingAddress = New System.Windows.Forms.Label()
        Me.lblHeadingPhone = New System.Windows.Forms.Label()
        Me.lblHeadingFirstName = New System.Windows.Forms.Label()
        Me.lblHeadingGender = New System.Windows.Forms.Label()
        Me.txtTitle = New System.Windows.Forms.TextBox()
        Me.lblHeadingTitle = New System.Windows.Forms.Label()
        Me.txtCustomerID = New System.Windows.Forms.TextBox()
        Me.lblHeadingCustomerID = New System.Windows.Forms.Label()
        Me.btnInsert = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.btnFirst = New System.Windows.Forms.Button()
        Me.btnLast = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.picErrorCustomerId, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorDOB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorAddress, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorPhone, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorLastName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorFirstName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorGender, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorTitle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpDOB)
        Me.GroupBox1.Controls.Add(Me.cbGender)
        Me.GroupBox1.Controls.Add(Me.picErrorCustomerId)
        Me.GroupBox1.Controls.Add(Me.picErrorDOB)
        Me.GroupBox1.Controls.Add(Me.lblHeadingDOB)
        Me.GroupBox1.Controls.Add(Me.picErrorEmail)
        Me.GroupBox1.Controls.Add(Me.txtEmail)
        Me.GroupBox1.Controls.Add(Me.lblHeadingEmail)
        Me.GroupBox1.Controls.Add(Me.picErrorAddress)
        Me.GroupBox1.Controls.Add(Me.picErrorPhone)
        Me.GroupBox1.Controls.Add(Me.picErrorLastName)
        Me.GroupBox1.Controls.Add(Me.picErrorFirstName)
        Me.GroupBox1.Controls.Add(Me.picErrorGender)
        Me.GroupBox1.Controls.Add(Me.picErrorTitle)
        Me.GroupBox1.Controls.Add(Me.txtAddress)
        Me.GroupBox1.Controls.Add(Me.txtPhone)
        Me.GroupBox1.Controls.Add(Me.txtLastName)
        Me.GroupBox1.Controls.Add(Me.txtFirstName)
        Me.GroupBox1.Controls.Add(Me.lblHeadingLastName)
        Me.GroupBox1.Controls.Add(Me.lblHeadingAddress)
        Me.GroupBox1.Controls.Add(Me.lblHeadingPhone)
        Me.GroupBox1.Controls.Add(Me.lblHeadingFirstName)
        Me.GroupBox1.Controls.Add(Me.lblHeadingGender)
        Me.GroupBox1.Controls.Add(Me.txtTitle)
        Me.GroupBox1.Controls.Add(Me.lblHeadingTitle)
        Me.GroupBox1.Controls.Add(Me.txtCustomerID)
        Me.GroupBox1.Controls.Add(Me.lblHeadingCustomerID)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(380, 287)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Customer Details"
        '
        'dtpDOB
        '
        Me.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDOB.Location = New System.Drawing.Point(146, 247)
        Me.dtpDOB.Name = "dtpDOB"
        Me.dtpDOB.Size = New System.Drawing.Size(200, 20)
        Me.dtpDOB.TabIndex = 10
        '
        'cbGender
        '
        Me.cbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbGender.FormattingEnabled = True
        Me.cbGender.Items.AddRange(New Object() {"male", "female"})
        Me.cbGender.Location = New System.Drawing.Point(146, 76)
        Me.cbGender.Name = "cbGender"
        Me.cbGender.Size = New System.Drawing.Size(202, 21)
        Me.cbGender.TabIndex = 5
        '
        'picErrorCustomerId
        '
        Me.picErrorCustomerId.ErrorImage = Nothing
        Me.picErrorCustomerId.Image = CType(resources.GetObject("picErrorCustomerId.Image"), System.Drawing.Image)
        Me.picErrorCustomerId.Location = New System.Drawing.Point(354, 19)
        Me.picErrorCustomerId.Name = "picErrorCustomerId"
        Me.picErrorCustomerId.Size = New System.Drawing.Size(25, 20)
        Me.picErrorCustomerId.TabIndex = 28
        Me.picErrorCustomerId.TabStop = False
        Me.picErrorCustomerId.Visible = False
        '
        'picErrorDOB
        '
        Me.picErrorDOB.ErrorImage = Nothing
        Me.picErrorDOB.Image = CType(resources.GetObject("picErrorDOB.Image"), System.Drawing.Image)
        Me.picErrorDOB.Location = New System.Drawing.Point(354, 247)
        Me.picErrorDOB.Name = "picErrorDOB"
        Me.picErrorDOB.Size = New System.Drawing.Size(25, 20)
        Me.picErrorDOB.TabIndex = 27
        Me.picErrorDOB.TabStop = False
        Me.picErrorDOB.Visible = False
        '
        'lblHeadingDOB
        '
        Me.lblHeadingDOB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingDOB.Location = New System.Drawing.Point(6, 246)
        Me.lblHeadingDOB.Name = "lblHeadingDOB"
        Me.lblHeadingDOB.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingDOB.TabIndex = 25
        Me.lblHeadingDOB.Text = "DOB"
        Me.lblHeadingDOB.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'picErrorEmail
        '
        Me.picErrorEmail.ErrorImage = Nothing
        Me.picErrorEmail.Image = CType(resources.GetObject("picErrorEmail.Image"), System.Drawing.Image)
        Me.picErrorEmail.Location = New System.Drawing.Point(354, 221)
        Me.picErrorEmail.Name = "picErrorEmail"
        Me.picErrorEmail.Size = New System.Drawing.Size(25, 20)
        Me.picErrorEmail.TabIndex = 24
        Me.picErrorEmail.TabStop = False
        Me.picErrorEmail.Visible = False
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(146, 221)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(202, 20)
        Me.txtEmail.TabIndex = 23
        '
        'lblHeadingEmail
        '
        Me.lblHeadingEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingEmail.Location = New System.Drawing.Point(6, 220)
        Me.lblHeadingEmail.Name = "lblHeadingEmail"
        Me.lblHeadingEmail.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingEmail.TabIndex = 22
        Me.lblHeadingEmail.Text = "Email"
        Me.lblHeadingEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'picErrorAddress
        '
        Me.picErrorAddress.ErrorImage = Nothing
        Me.picErrorAddress.Image = CType(resources.GetObject("picErrorAddress.Image"), System.Drawing.Image)
        Me.picErrorAddress.Location = New System.Drawing.Point(354, 195)
        Me.picErrorAddress.Name = "picErrorAddress"
        Me.picErrorAddress.Size = New System.Drawing.Size(25, 20)
        Me.picErrorAddress.TabIndex = 21
        Me.picErrorAddress.TabStop = False
        Me.picErrorAddress.Visible = False
        '
        'picErrorPhone
        '
        Me.picErrorPhone.ErrorImage = Nothing
        Me.picErrorPhone.Image = CType(resources.GetObject("picErrorPhone.Image"), System.Drawing.Image)
        Me.picErrorPhone.Location = New System.Drawing.Point(354, 165)
        Me.picErrorPhone.Name = "picErrorPhone"
        Me.picErrorPhone.Size = New System.Drawing.Size(25, 20)
        Me.picErrorPhone.TabIndex = 20
        Me.picErrorPhone.TabStop = False
        Me.picErrorPhone.Visible = False
        '
        'picErrorLastName
        '
        Me.picErrorLastName.ErrorImage = Nothing
        Me.picErrorLastName.Image = CType(resources.GetObject("picErrorLastName.Image"), System.Drawing.Image)
        Me.picErrorLastName.Location = New System.Drawing.Point(354, 133)
        Me.picErrorLastName.Name = "picErrorLastName"
        Me.picErrorLastName.Size = New System.Drawing.Size(25, 20)
        Me.picErrorLastName.TabIndex = 19
        Me.picErrorLastName.TabStop = False
        Me.picErrorLastName.Visible = False
        '
        'picErrorFirstName
        '
        Me.picErrorFirstName.ErrorImage = Nothing
        Me.picErrorFirstName.Image = CType(resources.GetObject("picErrorFirstName.Image"), System.Drawing.Image)
        Me.picErrorFirstName.Location = New System.Drawing.Point(354, 107)
        Me.picErrorFirstName.Name = "picErrorFirstName"
        Me.picErrorFirstName.Size = New System.Drawing.Size(25, 20)
        Me.picErrorFirstName.TabIndex = 18
        Me.picErrorFirstName.TabStop = False
        Me.picErrorFirstName.Visible = False
        '
        'picErrorGender
        '
        Me.picErrorGender.ErrorImage = Nothing
        Me.picErrorGender.Image = CType(resources.GetObject("picErrorGender.Image"), System.Drawing.Image)
        Me.picErrorGender.Location = New System.Drawing.Point(354, 75)
        Me.picErrorGender.Name = "picErrorGender"
        Me.picErrorGender.Size = New System.Drawing.Size(25, 20)
        Me.picErrorGender.TabIndex = 17
        Me.picErrorGender.TabStop = False
        Me.picErrorGender.Visible = False
        '
        'picErrorTitle
        '
        Me.picErrorTitle.ErrorImage = Nothing
        Me.picErrorTitle.Image = CType(resources.GetObject("picErrorTitle.Image"), System.Drawing.Image)
        Me.picErrorTitle.Location = New System.Drawing.Point(354, 45)
        Me.picErrorTitle.Name = "picErrorTitle"
        Me.picErrorTitle.Size = New System.Drawing.Size(25, 20)
        Me.picErrorTitle.TabIndex = 16
        Me.picErrorTitle.TabStop = False
        Me.picErrorTitle.Visible = False
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(146, 195)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(202, 20)
        Me.txtAddress.TabIndex = 14
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(146, 165)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(202, 20)
        Me.txtPhone.TabIndex = 13
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(146, 133)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(202, 20)
        Me.txtLastName.TabIndex = 12
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(146, 106)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(202, 20)
        Me.txtFirstName.TabIndex = 11
        '
        'lblHeadingLastName
        '
        Me.lblHeadingLastName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingLastName.Location = New System.Drawing.Point(6, 132)
        Me.lblHeadingLastName.Name = "lblHeadingLastName"
        Me.lblHeadingLastName.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingLastName.TabIndex = 9
        Me.lblHeadingLastName.Text = "Last Name"
        Me.lblHeadingLastName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingAddress
        '
        Me.lblHeadingAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingAddress.Location = New System.Drawing.Point(6, 194)
        Me.lblHeadingAddress.Name = "lblHeadingAddress"
        Me.lblHeadingAddress.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingAddress.TabIndex = 7
        Me.lblHeadingAddress.Text = "Address"
        Me.lblHeadingAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingPhone
        '
        Me.lblHeadingPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingPhone.Location = New System.Drawing.Point(6, 164)
        Me.lblHeadingPhone.Name = "lblHeadingPhone"
        Me.lblHeadingPhone.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingPhone.TabIndex = 6
        Me.lblHeadingPhone.Text = "Phone"
        Me.lblHeadingPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingFirstName
        '
        Me.lblHeadingFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingFirstName.Location = New System.Drawing.Point(6, 105)
        Me.lblHeadingFirstName.Name = "lblHeadingFirstName"
        Me.lblHeadingFirstName.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingFirstName.TabIndex = 5
        Me.lblHeadingFirstName.Text = "First Name"
        Me.lblHeadingFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingGender
        '
        Me.lblHeadingGender.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingGender.Location = New System.Drawing.Point(6, 75)
        Me.lblHeadingGender.Name = "lblHeadingGender"
        Me.lblHeadingGender.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingGender.TabIndex = 4
        Me.lblHeadingGender.Text = "Gender"
        Me.lblHeadingGender.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtTitle
        '
        Me.txtTitle.Location = New System.Drawing.Point(146, 46)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(202, 20)
        Me.txtTitle.TabIndex = 3
        '
        'lblHeadingTitle
        '
        Me.lblHeadingTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingTitle.Location = New System.Drawing.Point(6, 45)
        Me.lblHeadingTitle.Name = "lblHeadingTitle"
        Me.lblHeadingTitle.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingTitle.TabIndex = 2
        Me.lblHeadingTitle.Text = "Title"
        Me.lblHeadingTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCustomerID
        '
        Me.txtCustomerID.Location = New System.Drawing.Point(146, 19)
        Me.txtCustomerID.Name = "txtCustomerID"
        Me.txtCustomerID.Size = New System.Drawing.Size(202, 20)
        Me.txtCustomerID.TabIndex = 1
        '
        'lblHeadingCustomerID
        '
        Me.lblHeadingCustomerID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingCustomerID.Location = New System.Drawing.Point(6, 16)
        Me.lblHeadingCustomerID.Name = "lblHeadingCustomerID"
        Me.lblHeadingCustomerID.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingCustomerID.TabIndex = 1
        Me.lblHeadingCustomerID.Text = "Customer ID"
        Me.lblHeadingCustomerID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnInsert
        '
        Me.btnInsert.Location = New System.Drawing.Point(398, 31)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(215, 20)
        Me.btnInsert.TabIndex = 1
        Me.btnInsert.Text = "Insert"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(398, 88)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(215, 20)
        Me.btnUpdate.TabIndex = 2
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(398, 114)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(215, 20)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnFind
        '
        Me.btnFind.Location = New System.Drawing.Point(398, 58)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(215, 20)
        Me.btnFind.TabIndex = 4
        Me.btnFind.Text = "Find"
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(398, 177)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(215, 20)
        Me.btnClear.TabIndex = 5
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(513, 148)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(30, 23)
        Me.btnNext.TabIndex = 6
        Me.btnNext.Text = ">"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.Location = New System.Drawing.Point(477, 148)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(30, 23)
        Me.btnPrev.TabIndex = 7
        Me.btnPrev.Text = "<"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btnFirst
        '
        Me.btnFirst.Location = New System.Drawing.Point(441, 148)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(30, 23)
        Me.btnFirst.TabIndex = 8
        Me.btnFirst.Text = "|<"
        Me.btnFirst.UseVisualStyleBackColor = True
        '
        'btnLast
        '
        Me.btnLast.Location = New System.Drawing.Point(549, 148)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(30, 23)
        Me.btnLast.TabIndex = 9
        Me.btnLast.Text = ">|"
        Me.btnLast.UseVisualStyleBackColor = True
        '
        'frmCustomer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(624, 314)
        Me.Controls.Add(Me.btnLast)
        Me.Controls.Add(Me.btnFirst)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmCustomer"
        Me.Text = "customer"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.picErrorCustomerId, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorDOB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorAddress, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorPhone, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorLastName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorFirstName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorGender, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorTitle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtPhone As System.Windows.Forms.TextBox
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadingLastName As System.Windows.Forms.Label
    Friend WithEvents lblHeadingAddress As System.Windows.Forms.Label
    Friend WithEvents lblHeadingPhone As System.Windows.Forms.Label
    Friend WithEvents lblHeadingFirstName As System.Windows.Forms.Label
    Friend WithEvents lblHeadingGender As System.Windows.Forms.Label
    Friend WithEvents txtTitle As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadingTitle As System.Windows.Forms.Label
    Friend WithEvents txtCustomerID As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadingCustomerID As System.Windows.Forms.Label
    Friend WithEvents btnInsert As System.Windows.Forms.Button
    Friend WithEvents picErrorAddress As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorPhone As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorLastName As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorFirstName As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorGender As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorTitle As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorDOB As System.Windows.Forms.PictureBox
    Friend WithEvents lblHeadingDOB As System.Windows.Forms.Label
    Friend WithEvents picErrorEmail As System.Windows.Forms.PictureBox
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadingEmail As System.Windows.Forms.Label
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents picErrorCustomerId As System.Windows.Forms.PictureBox
    Friend WithEvents cbGender As System.Windows.Forms.ComboBox
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btnFirst As System.Windows.Forms.Button
    Friend WithEvents btnLast As System.Windows.Forms.Button
    Friend WithEvents dtpDOB As System.Windows.Forms.DateTimePicker
End Class
