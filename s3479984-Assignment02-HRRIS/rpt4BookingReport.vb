﻿Option Explicit On
Option Strict On

Public Class rpt4BookingReport

    Private Sub frmComboBox_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'change cbo to dropdownlist
        cboMonth.DropDownStyle = ComboBoxStyle.DropDownList

    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnGenerateReport.Click
        Dim oController As rpt4BookingReportController = New rpt4BookingReportController

        Dim sMonth = cboMonth.SelectedItem
        Dim sYear = txtYear.Text
        oController.createReport(CStr(sMonth), CStr(sYear))

    End Sub

End Class