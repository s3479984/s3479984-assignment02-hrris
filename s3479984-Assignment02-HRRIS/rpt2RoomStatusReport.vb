﻿Option Explicit On
Option Strict On

Public Class rpt2RoomStatusReport

    'init public variable for room id
    Dim lsDataRoom As List(Of Hashtable)
    Public iRoomID As Integer

    Private Sub frmComboBox_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboRoomNumber.DropDownStyle = ComboBoxStyle.DropDownList
        Dim oBookingController As BookingController = New BookingController

        lsDataRoom = oBookingController.findRoom()
        For Each room In lsDataRoom
            cboRoomNumber.Items.Add(CStr(room("RoomNumber")))
        Next

    End Sub

    Private Sub cboRoomNumber_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRoomNumber.SelectedIndexChanged

        Dim selectedIndex As Integer = cboRoomNumber.SelectedIndex
        Dim selectedItem As Object = cboRoomNumber.SelectedItem

        Dim htTempData = lsDataRoom.Item(selectedIndex)
        iRoomID = CInt(CStr(htTempData("RoomID")))

    End Sub

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnGenerateReport.Click
        Dim oController As rpt2RoomStatusReportController = New rpt2RoomStatusReportController
        Dim sId = iRoomID
        oController.createReport(CStr(sId))

    End Sub

End Class