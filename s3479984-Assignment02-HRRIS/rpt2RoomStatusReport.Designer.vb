﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class rpt2RoomStatusReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblRoomNumber = New System.Windows.Forms.Label()
        Me.btnGenerateReport = New System.Windows.Forms.Button()
        Me.cboRoomNumber = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'lblRoomNumber
        '
        Me.lblRoomNumber.AutoSize = True
        Me.lblRoomNumber.Location = New System.Drawing.Point(40, 91)
        Me.lblRoomNumber.Name = "lblRoomNumber"
        Me.lblRoomNumber.Size = New System.Drawing.Size(75, 13)
        Me.lblRoomNumber.TabIndex = 5
        Me.lblRoomNumber.Text = "Room Number"
        '
        'btnGenerateReport
        '
        Me.btnGenerateReport.Location = New System.Drawing.Point(80, 150)
        Me.btnGenerateReport.Name = "btnGenerateReport"
        Me.btnGenerateReport.Size = New System.Drawing.Size(115, 23)
        Me.btnGenerateReport.TabIndex = 4
        Me.btnGenerateReport.Text = "Generate Report"
        Me.btnGenerateReport.UseVisualStyleBackColor = True
        '
        'cboRoomNumber
        '
        Me.cboRoomNumber.FormattingEnabled = True
        Me.cboRoomNumber.Location = New System.Drawing.Point(124, 88)
        Me.cboRoomNumber.Name = "cboRoomNumber"
        Me.cboRoomNumber.Size = New System.Drawing.Size(121, 21)
        Me.cboRoomNumber.TabIndex = 3
        '
        'rpt2RoomStatusReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.lblRoomNumber)
        Me.Controls.Add(Me.btnGenerateReport)
        Me.Controls.Add(Me.cboRoomNumber)
        Me.Name = "rpt2RoomStatusReport"
        Me.Text = "rpt2RoomStatusReport"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblRoomNumber As System.Windows.Forms.Label
    Friend WithEvents btnGenerateReport As System.Windows.Forms.Button
    Friend WithEvents cboRoomNumber As System.Windows.Forms.ComboBox
End Class
