﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRoom
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRoom))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkAvailability = New System.Windows.Forms.CheckBox()
        Me.picErrorRoomID = New System.Windows.Forms.PictureBox()
        Me.picErrorDescription = New System.Windows.Forms.PictureBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.lblHeadinDescription = New System.Windows.Forms.Label()
        Me.picErrorFloor = New System.Windows.Forms.PictureBox()
        Me.picErrorNumBeds = New System.Windows.Forms.PictureBox()
        Me.picErrorPrice = New System.Windows.Forms.PictureBox()
        Me.picErrorType = New System.Windows.Forms.PictureBox()
        Me.picErrorRoomNumber = New System.Windows.Forms.PictureBox()
        Me.txtFloor = New System.Windows.Forms.TextBox()
        Me.txtNumBeds = New System.Windows.Forms.TextBox()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.txtType = New System.Windows.Forms.TextBox()
        Me.lblHeadingNumBeds = New System.Windows.Forms.Label()
        Me.lblHeadingFloor = New System.Windows.Forms.Label()
        Me.lblHeadingAvailability = New System.Windows.Forms.Label()
        Me.lblHeadingPrice = New System.Windows.Forms.Label()
        Me.lblHeadingType = New System.Windows.Forms.Label()
        Me.txtRoomNumber = New System.Windows.Forms.TextBox()
        Me.lblHeadingTitle = New System.Windows.Forms.Label()
        Me.txtRoomID = New System.Windows.Forms.TextBox()
        Me.lblHeadingRoomID = New System.Windows.Forms.Label()
        Me.btnInsert = New System.Windows.Forms.Button()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnLast = New System.Windows.Forms.Button()
        Me.btnFirst = New System.Windows.Forms.Button()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.picErrorRoomID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorDescription, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorFloor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorNumBeds, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorPrice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picErrorRoomNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkAvailability)
        Me.GroupBox1.Controls.Add(Me.picErrorRoomID)
        Me.GroupBox1.Controls.Add(Me.picErrorDescription)
        Me.GroupBox1.Controls.Add(Me.txtDescription)
        Me.GroupBox1.Controls.Add(Me.lblHeadinDescription)
        Me.GroupBox1.Controls.Add(Me.picErrorFloor)
        Me.GroupBox1.Controls.Add(Me.picErrorNumBeds)
        Me.GroupBox1.Controls.Add(Me.picErrorPrice)
        Me.GroupBox1.Controls.Add(Me.picErrorType)
        Me.GroupBox1.Controls.Add(Me.picErrorRoomNumber)
        Me.GroupBox1.Controls.Add(Me.txtFloor)
        Me.GroupBox1.Controls.Add(Me.txtNumBeds)
        Me.GroupBox1.Controls.Add(Me.txtPrice)
        Me.GroupBox1.Controls.Add(Me.txtType)
        Me.GroupBox1.Controls.Add(Me.lblHeadingNumBeds)
        Me.GroupBox1.Controls.Add(Me.lblHeadingFloor)
        Me.GroupBox1.Controls.Add(Me.lblHeadingAvailability)
        Me.GroupBox1.Controls.Add(Me.lblHeadingPrice)
        Me.GroupBox1.Controls.Add(Me.lblHeadingType)
        Me.GroupBox1.Controls.Add(Me.txtRoomNumber)
        Me.GroupBox1.Controls.Add(Me.lblHeadingTitle)
        Me.GroupBox1.Controls.Add(Me.txtRoomID)
        Me.GroupBox1.Controls.Add(Me.lblHeadingRoomID)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(380, 258)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Room Details"
        '
        'chkAvailability
        '
        Me.chkAvailability.AutoSize = True
        Me.chkAvailability.Location = New System.Drawing.Point(146, 166)
        Me.chkAvailability.Name = "chkAvailability"
        Me.chkAvailability.Size = New System.Drawing.Size(15, 14)
        Me.chkAvailability.TabIndex = 26
        Me.chkAvailability.UseVisualStyleBackColor = True
        '
        'picErrorRoomID
        '
        Me.picErrorRoomID.ErrorImage = CType(resources.GetObject("picErrorRoomID.ErrorImage"), System.Drawing.Image)
        Me.picErrorRoomID.Image = CType(resources.GetObject("picErrorRoomID.Image"), System.Drawing.Image)
        Me.picErrorRoomID.Location = New System.Drawing.Point(354, 19)
        Me.picErrorRoomID.Name = "picErrorRoomID"
        Me.picErrorRoomID.Size = New System.Drawing.Size(25, 20)
        Me.picErrorRoomID.TabIndex = 25
        Me.picErrorRoomID.TabStop = False
        Me.picErrorRoomID.Visible = False
        '
        'picErrorDescription
        '
        Me.picErrorDescription.ErrorImage = CType(resources.GetObject("picErrorDescription.ErrorImage"), System.Drawing.Image)
        Me.picErrorDescription.Image = CType(resources.GetObject("picErrorDescription.Image"), System.Drawing.Image)
        Me.picErrorDescription.Location = New System.Drawing.Point(354, 221)
        Me.picErrorDescription.Name = "picErrorDescription"
        Me.picErrorDescription.Size = New System.Drawing.Size(25, 20)
        Me.picErrorDescription.TabIndex = 24
        Me.picErrorDescription.TabStop = False
        Me.picErrorDescription.Visible = False
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(146, 221)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(202, 20)
        Me.txtDescription.TabIndex = 23
        '
        'lblHeadinDescription
        '
        Me.lblHeadinDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadinDescription.Location = New System.Drawing.Point(6, 220)
        Me.lblHeadinDescription.Name = "lblHeadinDescription"
        Me.lblHeadinDescription.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadinDescription.TabIndex = 22
        Me.lblHeadinDescription.Text = "Description"
        Me.lblHeadinDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'picErrorFloor
        '
        Me.picErrorFloor.ErrorImage = CType(resources.GetObject("picErrorFloor.ErrorImage"), System.Drawing.Image)
        Me.picErrorFloor.Image = CType(resources.GetObject("picErrorFloor.Image"), System.Drawing.Image)
        Me.picErrorFloor.Location = New System.Drawing.Point(354, 195)
        Me.picErrorFloor.Name = "picErrorFloor"
        Me.picErrorFloor.Size = New System.Drawing.Size(25, 20)
        Me.picErrorFloor.TabIndex = 21
        Me.picErrorFloor.TabStop = False
        Me.picErrorFloor.Visible = False
        '
        'picErrorNumBeds
        '
        Me.picErrorNumBeds.ErrorImage = CType(resources.GetObject("picErrorNumBeds.ErrorImage"), System.Drawing.Image)
        Me.picErrorNumBeds.Image = CType(resources.GetObject("picErrorNumBeds.Image"), System.Drawing.Image)
        Me.picErrorNumBeds.Location = New System.Drawing.Point(354, 133)
        Me.picErrorNumBeds.Name = "picErrorNumBeds"
        Me.picErrorNumBeds.Size = New System.Drawing.Size(25, 20)
        Me.picErrorNumBeds.TabIndex = 19
        Me.picErrorNumBeds.TabStop = False
        Me.picErrorNumBeds.Visible = False
        '
        'picErrorPrice
        '
        Me.picErrorPrice.ErrorImage = CType(resources.GetObject("picErrorPrice.ErrorImage"), System.Drawing.Image)
        Me.picErrorPrice.Image = CType(resources.GetObject("picErrorPrice.Image"), System.Drawing.Image)
        Me.picErrorPrice.Location = New System.Drawing.Point(354, 107)
        Me.picErrorPrice.Name = "picErrorPrice"
        Me.picErrorPrice.Size = New System.Drawing.Size(25, 20)
        Me.picErrorPrice.TabIndex = 18
        Me.picErrorPrice.TabStop = False
        Me.picErrorPrice.Visible = False
        '
        'picErrorType
        '
        Me.picErrorType.ErrorImage = CType(resources.GetObject("picErrorType.ErrorImage"), System.Drawing.Image)
        Me.picErrorType.Image = CType(resources.GetObject("picErrorType.Image"), System.Drawing.Image)
        Me.picErrorType.Location = New System.Drawing.Point(354, 75)
        Me.picErrorType.Name = "picErrorType"
        Me.picErrorType.Size = New System.Drawing.Size(25, 20)
        Me.picErrorType.TabIndex = 17
        Me.picErrorType.TabStop = False
        Me.picErrorType.Visible = False
        '
        'picErrorRoomNumber
        '
        Me.picErrorRoomNumber.ErrorImage = CType(resources.GetObject("picErrorRoomNumber.ErrorImage"), System.Drawing.Image)
        Me.picErrorRoomNumber.Image = CType(resources.GetObject("picErrorRoomNumber.Image"), System.Drawing.Image)
        Me.picErrorRoomNumber.Location = New System.Drawing.Point(354, 45)
        Me.picErrorRoomNumber.Name = "picErrorRoomNumber"
        Me.picErrorRoomNumber.Size = New System.Drawing.Size(25, 20)
        Me.picErrorRoomNumber.TabIndex = 16
        Me.picErrorRoomNumber.TabStop = False
        Me.picErrorRoomNumber.Visible = False
        '
        'txtFloor
        '
        Me.txtFloor.Location = New System.Drawing.Point(146, 195)
        Me.txtFloor.Name = "txtFloor"
        Me.txtFloor.Size = New System.Drawing.Size(202, 20)
        Me.txtFloor.TabIndex = 14
        '
        'txtNumBeds
        '
        Me.txtNumBeds.Location = New System.Drawing.Point(146, 133)
        Me.txtNumBeds.Name = "txtNumBeds"
        Me.txtNumBeds.Size = New System.Drawing.Size(202, 20)
        Me.txtNumBeds.TabIndex = 12
        '
        'txtPrice
        '
        Me.txtPrice.Location = New System.Drawing.Point(146, 106)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(202, 20)
        Me.txtPrice.TabIndex = 11
        '
        'txtType
        '
        Me.txtType.Location = New System.Drawing.Point(146, 76)
        Me.txtType.Name = "txtType"
        Me.txtType.Size = New System.Drawing.Size(202, 20)
        Me.txtType.TabIndex = 10
        '
        'lblHeadingNumBeds
        '
        Me.lblHeadingNumBeds.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingNumBeds.Location = New System.Drawing.Point(6, 132)
        Me.lblHeadingNumBeds.Name = "lblHeadingNumBeds"
        Me.lblHeadingNumBeds.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingNumBeds.TabIndex = 9
        Me.lblHeadingNumBeds.Text = "Number Beds"
        Me.lblHeadingNumBeds.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingFloor
        '
        Me.lblHeadingFloor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingFloor.Location = New System.Drawing.Point(6, 194)
        Me.lblHeadingFloor.Name = "lblHeadingFloor"
        Me.lblHeadingFloor.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingFloor.TabIndex = 7
        Me.lblHeadingFloor.Text = "Floor"
        Me.lblHeadingFloor.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingAvailability
        '
        Me.lblHeadingAvailability.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingAvailability.Location = New System.Drawing.Point(6, 164)
        Me.lblHeadingAvailability.Name = "lblHeadingAvailability"
        Me.lblHeadingAvailability.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingAvailability.TabIndex = 6
        Me.lblHeadingAvailability.Text = "Availability"
        Me.lblHeadingAvailability.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingPrice
        '
        Me.lblHeadingPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingPrice.Location = New System.Drawing.Point(6, 105)
        Me.lblHeadingPrice.Name = "lblHeadingPrice"
        Me.lblHeadingPrice.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingPrice.TabIndex = 5
        Me.lblHeadingPrice.Text = "Price"
        Me.lblHeadingPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHeadingType
        '
        Me.lblHeadingType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingType.Location = New System.Drawing.Point(6, 75)
        Me.lblHeadingType.Name = "lblHeadingType"
        Me.lblHeadingType.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingType.TabIndex = 4
        Me.lblHeadingType.Text = "Type"
        Me.lblHeadingType.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRoomNumber
        '
        Me.txtRoomNumber.Location = New System.Drawing.Point(146, 46)
        Me.txtRoomNumber.Name = "txtRoomNumber"
        Me.txtRoomNumber.Size = New System.Drawing.Size(202, 20)
        Me.txtRoomNumber.TabIndex = 3
        '
        'lblHeadingTitle
        '
        Me.lblHeadingTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingTitle.Location = New System.Drawing.Point(6, 45)
        Me.lblHeadingTitle.Name = "lblHeadingTitle"
        Me.lblHeadingTitle.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingTitle.TabIndex = 2
        Me.lblHeadingTitle.Text = "Room Number"
        Me.lblHeadingTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRoomID
        '
        Me.txtRoomID.Location = New System.Drawing.Point(146, 19)
        Me.txtRoomID.Name = "txtRoomID"
        Me.txtRoomID.Size = New System.Drawing.Size(202, 20)
        Me.txtRoomID.TabIndex = 1
        '
        'lblHeadingRoomID
        '
        Me.lblHeadingRoomID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHeadingRoomID.Location = New System.Drawing.Point(6, 16)
        Me.lblHeadingRoomID.Name = "lblHeadingRoomID"
        Me.lblHeadingRoomID.Size = New System.Drawing.Size(125, 20)
        Me.lblHeadingRoomID.TabIndex = 1
        Me.lblHeadingRoomID.Text = "Room ID"
        Me.lblHeadingRoomID.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnInsert
        '
        Me.btnInsert.Location = New System.Drawing.Point(398, 31)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(215, 20)
        Me.btnInsert.TabIndex = 1
        Me.btnInsert.Text = "Insert"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'btnFind
        '
        Me.btnFind.Location = New System.Drawing.Point(398, 58)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(215, 20)
        Me.btnFind.TabIndex = 7
        Me.btnFind.Text = "Find"
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(398, 114)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(215, 20)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(398, 88)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(215, 20)
        Me.btnUpdate.TabIndex = 5
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(398, 178)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(215, 20)
        Me.btnClear.TabIndex = 8
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnLast
        '
        Me.btnLast.Location = New System.Drawing.Point(547, 144)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(30, 23)
        Me.btnLast.TabIndex = 13
        Me.btnLast.Text = ">|"
        Me.btnLast.UseVisualStyleBackColor = True
        '
        'btnFirst
        '
        Me.btnFirst.Location = New System.Drawing.Point(439, 144)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(30, 23)
        Me.btnFirst.TabIndex = 12
        Me.btnFirst.Text = "|<"
        Me.btnFirst.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.Location = New System.Drawing.Point(475, 144)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(30, 23)
        Me.btnPrev.TabIndex = 11
        Me.btnPrev.Text = "<"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(511, 144)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(30, 23)
        Me.btnNext.TabIndex = 10
        Me.btnNext.Text = ">"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'frmRoom
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(624, 289)
        Me.Controls.Add(Me.btnLast)
        Me.Controls.Add(Me.btnFirst)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnFind)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnInsert)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmRoom"
        Me.Text = "room"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.picErrorRoomID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorDescription, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorFloor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorNumBeds, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorPrice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picErrorRoomNumber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFloor As System.Windows.Forms.TextBox
    Friend WithEvents txtNumBeds As System.Windows.Forms.TextBox
    Friend WithEvents txtPrice As System.Windows.Forms.TextBox
    Friend WithEvents txtType As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadingNumBeds As System.Windows.Forms.Label
    Friend WithEvents lblHeadingFloor As System.Windows.Forms.Label
    Friend WithEvents lblHeadingAvailability As System.Windows.Forms.Label
    Friend WithEvents lblHeadingPrice As System.Windows.Forms.Label
    Friend WithEvents lblHeadingType As System.Windows.Forms.Label
    Friend WithEvents txtRoomNumber As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadingTitle As System.Windows.Forms.Label
    Friend WithEvents txtRoomID As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadingRoomID As System.Windows.Forms.Label
    Friend WithEvents btnInsert As System.Windows.Forms.Button
    Friend WithEvents picErrorFloor As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorNumBeds As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorPrice As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorType As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorRoomNumber As System.Windows.Forms.PictureBox
    Friend WithEvents picErrorDescription As System.Windows.Forms.PictureBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents lblHeadinDescription As System.Windows.Forms.Label
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents picErrorRoomID As System.Windows.Forms.PictureBox
    Friend WithEvents chkAvailability As System.Windows.Forms.CheckBox
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents btnLast As System.Windows.Forms.Button
    Friend WithEvents btnFirst As System.Windows.Forms.Button
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
End Class
