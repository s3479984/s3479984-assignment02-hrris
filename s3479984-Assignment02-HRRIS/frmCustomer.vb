﻿Option Explicit On
Option Strict On

Imports System.Drawing

Public Class frmCustomer

    'button insert'
    Private Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click

        Dim bIsValid = validateFormData()

        If bIsValid Then
            Dim htData As Hashtable = New Hashtable
            htData("Title") = txtTitle.Text
            htData("Gender") = cbGender.Text
            htData("FirstName") = txtFirstName.Text
            htData("LastName") = txtLastName.Text
            htData("Phone") = txtPhone.Text
            htData("Address") = txtAddress.Text
            htData("Email") = txtEmail.Text
            htData("DOB") = dtpDOB.Text

            Dim oCustomerController As CustomerController = New CustomerController
            oCustomerController.insert(htData)

        Else
            ' Do nothing in this case
        End If

    End Sub

    'validate the form'
    Private Function validateFormData() As Boolean

        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim tt As New ToolTip()

        'validate customer id
        bIsValid = oValidation.isNumericVal(txtCustomerID.Text)
        If bIsValid Then
            picErrorTitle.Visible = False
        Else
            picErrorCustomerId.Visible = True
            tt.SetToolTip(picErrorTitle, "Value is not numeric")
            bAllFieldsValid = False
        End If

        'validate title
        bIsValid = oValidation.isAlphaNumericVal(txtTitle.Text)
        If bIsValid Then
            picErrorTitle.Visible = False
        Else
            picErrorTitle.Visible = True
            tt.SetToolTip(picErrorTitle, "Value is not alphanumeric")
            bAllFieldsValid = False
        End If

        'validate gender
        bIsValid = oValidation.isAlphaNumericVal(cbGender.Text)
        If bIsValid Then
            picErrorGender.Visible = False
        Else
            picErrorGender.Visible = True
            tt.SetToolTip(picErrorGender, "Value is not alphanumeric")
            bAllFieldsValid = False
        End If

        'validate first name
        bIsValid = oValidation.isAlphaNumericVal(txtFirstName.Text)
        If bIsValid Then
            picErrorFirstName.Visible = False
        Else
            picErrorFirstName.Visible = True
            tt.SetToolTip(picErrorFirstName, "Value is not alphanumeric")
            bAllFieldsValid = False
        End If

        'validate last name
        bIsValid = oValidation.isAlphaNumericVal(txtLastName.Text)
        If bIsValid Then
            picErrorLastName.Visible = False
        Else
            picErrorLastName.Visible = True
            tt.SetToolTip(picErrorLastName, "Value is not alphanumeric")
            bAllFieldsValid = False
        End If

        'validate phone number
        bIsValid = oValidation.isPhoneNumber(txtPhone.Text)
        If bIsValid Then
            picErrorPhone.Visible = False
        Else
            picErrorPhone.Visible = True
            tt.SetToolTip(picErrorPhone, "Value is not phone number")
            bAllFieldsValid = False
        End If

        'validate address
        bIsValid = oValidation.isAlphaNumericVal(txtAddress.Text)
        If bIsValid Then
            picErrorAddress.Visible = False
        Else
            picErrorAddress.Visible = True
            tt.SetToolTip(picErrorAddress, "Value is not alphanumeric")
            bAllFieldsValid = False
        End If

        'validate email
        bIsValid = oValidation.isEmail(txtEmail.Text)
        If bIsValid Then
            picErrorEmail.Visible = False
        Else
            picErrorEmail.Visible = True
            tt.SetToolTip(picErrorAddress, "Value is not alphanumeric")
            bAllFieldsValid = False
        End If

        'validate dob
        'bIsValid = oValidation.isDate(dtpDOB.Text)
        'If bIsValid Then
        '    picErrorDOB.Visible = False
        'Else
        '    picErrorDOB.Visible = True
        '    tt.SetToolTip(picErrorPhone, "Value is not numeric")
        '    bAllFieldsValid = False
        'End If

        'pseudo check for continue the code
        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        Else
            MsgBox("One of the fields was invalid")
        End If

        Return bAllFieldsValid

    End Function

    'button find
    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click

        Dim oController As CustomerController = New CustomerController

        'find id will be top priority
        If txtCustomerID.Text.Trim.Length > 0 Then

            'find by id
            Dim sId = txtCustomerID.Text
            Dim lsData = oController.findById(sId)

            If lsData.Count = 1 Then
                populateFormFields(lsData.Item(0))
            Else
                Debug.Print("No records were found")
            End If

        ElseIf txtFirstName.Text.Trim.Length > 0 Then

            'find by firstname
            Dim sFn = txtFirstName.Text
            Dim lsDataFN = oController.findbyFirstName(sFn)

            'populate if only one record, popup listbox if >1 record
            If lsDataFN.Count = 1 Then

                populateFormFields(lsDataFN.Item(0))

            Else

                popupSearch.Show()

            End If

        Else
            Debug.Print("No records were found")
        End If

    End Sub

    'populate data for button find
    Private Sub populateFormFields(htdata As Hashtable)

        txtCustomerID.Text = CStr(htdata("CustomerID"))
        txtTitle.Text = CStr(htdata("Title"))
        cbGender.Text = CStr(htdata("Gender"))
        txtFirstName.Text = CStr(htdata("FirstName"))
        txtLastName.Text = CStr(htdata("LastName"))
        txtPhone.Text = CStr(htdata("Phone"))
        txtAddress.Text = CStr(htdata("Address"))
        txtEmail.Text = CStr(htdata("Email"))
        dtpDOB.Text = CStr(htdata("DOB"))

    End Sub

    'button update'
    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim oController As CustomerController = New CustomerController

        Dim bIsValid = validateFormData()

        If bIsValid Then
            Dim htData As Hashtable = New Hashtable
            htData("Title") = txtTitle.Text
            htData("Gender") = cbGender.Text
            htData("FirstName") = txtFirstName.Text
            htData("LastName") = txtLastName.Text
            htData("Phone") = txtPhone.Text
            htData("Address") = txtAddress.Text
            htData("Email") = txtEmail.Text
            htData("DOB") = dtpDOB.Text
            htData("CustomerID") = txtCustomerID.Text

            Dim iNumRows = oController.update(htData)

            If iNumRows = 1 Then
                Debug.Print("The record was updated. Use the find button to check ...")
            Else
                Debug.Print("The record was not updated!")
            End If

        Else
            'nothing to do here
        End If

    End Sub

    'button delete'
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim oController As CustomerController = New CustomerController
        Dim sId = txtCustomerID.Text
        Dim iNumRows = oController.delete(sId)

        If MsgBox("Are you sure?", MsgBoxStyle.YesNoCancel, "Title") = MsgBoxResult.Yes Then
            If iNumRows = 1 Then
                clearForm()
                Debug.Print("The record was deleted. Use the find button to check ...")
            Else
                Debug.Print("The record was not deleted!")
            End If
        End If

    End Sub

    'clear everything on the form
    Private Sub clearForm()
        txtCustomerID.Clear()
        txtTitle.Clear()
        txtFirstName.Clear()
        txtLastName.Clear()
        txtPhone.Clear()
        txtAddress.Clear()
        txtEmail.Clear()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        clearForm()
    End Sub

    'our initial value for our minimun value to be used later for navigation
    Public minval As Integer = 0
    'the integer thata will hold the maximum value
    Public maxval As Integer
    'init lsNavData as all data for navigation
    Public lsNavData As New List(Of Hashtable)

    'first record fill on load
    Private Sub frmCustomer_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim oController As CustomerController = New CustomerController
        Dim lsData = oController.firstRecord()

        If lsData.Count = 1 Then
            populateFormFields(lsData.Item(0))
        Else
            'do nothing
        End If

        'find all record for navigation
        Dim lsNavData = oController.findAll()
        maxval = lsNavData.Count()
    End Sub

    'next record
    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        Dim oController As CustomerController = New CustomerController
        Dim lsNavData = oController.findAll()

        If minval <> maxval - 1 Then
            'minval is incremented by 1
            minval = minval + 1
            'throw the value of minval to nav procedure
            populateFormFields(lsNavData.Item(minval))

        Else
            MsgBox("Last record!")
        End If

    End Sub

    'prev record
    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrev.Click

        Dim oController As CustomerController = New CustomerController
        Dim lsNavData = oController.findAll()

        'check if minval is still greater than zero
        If minval > 0 Then
            'minval will be decremented by one
            minval = minval - 1
            populateFormFields(lsNavData.Item(minval))

        ElseIf minval = -1 Then
            MsgBox("No results found!")

        ElseIf minval = 0 Then
            MsgBox("First Record")
            populateFormFields(lsNavData.Item(minval))
        End If

    End Sub

    'first record
    Private Sub btnFirst_Click(sender As Object, e As EventArgs) Handles btnFirst.Click
        Dim oController As CustomerController = New CustomerController
        Dim lsNavData = oController.findAll()

        If minval <> 0 Then
            minval = 0
            populateFormFields(lsNavData.Item(minval))
        End If

    End Sub

    'last record
    Private Sub btnLast_Click(sender As Object, e As EventArgs) Handles btnLast.Click

        Dim oController As CustomerController = New CustomerController
        Dim lsNavData = oController.findAll()

        If minval <> maxval Then
            minval = maxval - 1
            populateFormFields(lsNavData.Item(minval))
        End If

    End Sub
End Class
