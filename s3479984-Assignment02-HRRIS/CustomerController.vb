﻿Option Explicit On
Option Strict On

Imports System.Data.OleDb

Public Class CustomerController

    Public Const CONNECTION_STRING As String = _
    "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=s3479984-hotelbooking.accdb"

    'find all record
    Public Function findAll() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT * FROM customer;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("CustomerID") = CStr(oDataReader("customer_id"))
                htTempData("Title") = CStr(oDataReader("title"))
                htTempData("Gender") = CStr(oDataReader("gender"))
                htTempData("FirstName") = CStr(oDataReader("firstname"))
                htTempData("LastName") = CStr(oDataReader("lastname"))
                htTempData("Phone") = CStr(oDataReader("phone"))
                htTempData("Address") = CStr(oDataReader("address"))
                htTempData("Email") = CStr(oDataReader("email"))
                htTempData("DOB") = CStr(oDataReader("dob"))
                lsData.Add(htTempData)
            Loop

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'find the fisrt record
    Public Function firstRecord() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT TOP 1 * FROM customer;"
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("CustomerID") = CStr(oDataReader("customer_id"))
                htTempData("Title") = CStr(oDataReader("title"))
                htTempData("Gender") = CStr(oDataReader("gender"))
                htTempData("FirstName") = CStr(oDataReader("firstname"))
                htTempData("LastName") = CStr(oDataReader("lastname"))
                htTempData("Phone") = CStr(oDataReader("phone"))
                htTempData("Address") = CStr(oDataReader("address"))
                htTempData("Email") = CStr(oDataReader("email"))
                htTempData("DOB") = CStr(oDataReader("dob"))
                lsData.Add(htTempData)
            Loop

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'insert into the database'
    Public Function insert(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)
            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection
            oCommand.CommandText = _
                "INSERT INTO customer (title,gender,firstname,lastname,phone,address,email,dob) VALUES (?, ?, ?, ?, ?, ?, ?, ?);"

            oCommand.Parameters.Add("Title", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Gender", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("FirstName", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("LastName", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Phone", OleDbType.Integer, 15)
            oCommand.Parameters.Add("Address", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Email", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("DOB", OleDbType.VarChar, 255)

            oCommand.Parameters("Title").Value = CStr(htData("Title"))
            oCommand.Parameters("Gender").Value = CStr(htData("Gender"))
            oCommand.Parameters("FirstName").Value = CStr(htData("FirstName"))
            oCommand.Parameters("LastName").Value = CStr(htData("LastName"))
            oCommand.Parameters("Phone").Value = CInt(htData("Phone"))
            oCommand.Parameters("Address").Value = CStr(htData("Address"))
            oCommand.Parameters("Email").Value = CStr(htData("Email"))
            oCommand.Parameters("DOB").Value = CStr(htData("DOB"))

            oCommand.Prepare()

            iNumRows = oCommand.ExecuteNonQuery()
            Debug.Print(CStr(iNumRows))

            Debug.Print("The record was inserted.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occured. The record wasn't inserted.")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

    'Find the record by input ID from the form'
    Public Function findById(sId As String) As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT * FROM customer  WHERE customer_id = ?;"
            oCommand.Parameters.Add("customer_id", OleDbType.Integer, 8)
            oCommand.Parameters("customer_id").Value = CInt(sId)
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("CustomerID") = CStr(oDataReader("customer_id"))
                htTempData("Title") = CStr(oDataReader("title"))
                htTempData("Gender") = CStr(oDataReader("gender"))
                htTempData("FirstName") = CStr(oDataReader("firstname"))
                htTempData("LastName") = CStr(oDataReader("lastname"))
                htTempData("Phone") = CStr(oDataReader("phone"))
                htTempData("Address") = CStr(oDataReader("address"))
                htTempData("Email") = CStr(oDataReader("email"))
                htTempData("DOB") = CStr(oDataReader("dob"))
                lsData.Add(htTempData)
            Loop

            Debug.Print("The record was found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'Find records by first name'
    Public Function findbyFirstName(sFn As String) As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT * FROM customer WHERE firstname = ?;"
            oCommand.Parameters.Add("firstname", OleDbType.VarChar, 255)
            oCommand.Parameters("firstname").Value = CStr(sFn)
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("CustomerID") = CStr(oDataReader("customer_id"))
                htTempData("Title") = CStr(oDataReader("title"))
                htTempData("Gender") = CStr(oDataReader("gender"))
                htTempData("FirstName") = CStr(oDataReader("firstname"))
                htTempData("LastName") = CStr(oDataReader("lastname"))
                htTempData("Phone") = CStr(oDataReader("phone"))
                htTempData("Address") = CStr(oDataReader("address"))
                htTempData("Email") = CStr(oDataReader("email"))
                htTempData("DOB") = CStr(oDataReader("dob"))
                lsData.Add(htTempData)
            Loop

            Debug.Print("The record was found.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

    'Update'
    Public Function update(ByVal htData As Hashtable) As Integer

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'TODO
            oCommand.CommandText = _
                "UPDATE customer  SET title = ?, gender = ?, firstname = ?, lastname = ?, phone = ?, address = ?, email = ?, dob = ? WHERE customer_id = ?;"

            oCommand.Parameters.Add("Title", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Gender", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("FirstName", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("LastName", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Phone", OleDbType.Integer, 15)
            oCommand.Parameters.Add("Address", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("Email", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("DOB", OleDbType.VarChar, 255)
            oCommand.Parameters.Add("CustomerID", OleDbType.VarChar, 255)

            oCommand.Parameters("Title").Value = CStr(htData("Title"))
            oCommand.Parameters("Gender").Value = CStr(htData("Gender"))
            oCommand.Parameters("FirstName").Value = CStr(htData("FirstName"))
            oCommand.Parameters("LastName").Value = CStr(htData("LastName"))
            oCommand.Parameters("Phone").Value = CInt(htData("Phone"))
            oCommand.Parameters("Address").Value = CStr(htData("Address"))
            oCommand.Parameters("Email").Value = CStr(htData("Email"))
            oCommand.Parameters("DOB").Value = CStr(htData("DOB"))
            oCommand.Parameters("CustomerID").Value = CStr(htData("CustomerID"))

            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))
            Debug.Print("The record was updated.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record was not updated!")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows
    End Function

    'Delete'
    Public Function delete(sId As String) As Integer
        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim iNumRows As Integer

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            'TODO
            oCommand.CommandText = _
               "DELETE FROM customer WHERE customer_id = ?;"
            oCommand.Parameters.Add("customer_id", OleDbType.Integer, 8)
            oCommand.Parameters("customer_id").Value = CInt(sId)
            oCommand.Prepare()
            iNumRows = oCommand.ExecuteNonQuery()

            Debug.Print(CStr(iNumRows))
            Debug.Print("The record was deleted.")

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record was not deleted!")
        Finally
            oConnection.Close()
        End Try

        Return iNumRows

    End Function

    'find customer'
    Public Function findCustomer() As List(Of Hashtable)

        Dim oConnection As OleDbConnection = New OleDbConnection(CONNECTION_STRING)
        Dim lsData As New List(Of Hashtable)

        Try
            Debug.Print("Connection string: " & oConnection.ConnectionString)

            oConnection.Open()
            Dim oCommand As OleDbCommand = New OleDbCommand
            oCommand.Connection = oConnection

            oCommand.CommandText = _
                "SELECT customer_id, firstname, lastname FROM customer;"
            oCommand.Parameters.Add("CustomerID", OleDbType.Integer, 15)
            oCommand.Prepare()
            Dim oDataReader = oCommand.ExecuteReader()

            Dim htTempData As Hashtable
            Do While oDataReader.Read() = True
                htTempData = New Hashtable
                htTempData("CustomerID") = CStr(oDataReader("customer_id"))
                htTempData("FirstName") = CStr(oDataReader("firstname"))
                htTempData("LastName") = CStr(oDataReader("lastname"))
                lsData.Add(htTempData)
            Loop

        Catch ex As Exception
            Debug.Print("ERROR: " & ex.Message)
            MsgBox("An error occurred. The record(s) could not be found!")
        Finally
            oConnection.Close()
        End Try

        Return lsData

    End Function

End Class
