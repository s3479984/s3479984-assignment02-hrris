﻿Option Explicit On
Option Strict On

Imports System.Drawing

Public Class frmRoom

    'button insert'
    Private Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click

        Dim bIsValid = validateFormData()

        If bIsValid Then
            Dim htData As Hashtable = New Hashtable
            htData("RoomNumber") = txtRoomNumber.Text
            htData("Type") = txtType.Text
            htData("Price") = txtPrice.Text
            htData("NumBeds") = txtNumBeds.Text
            If chkAvailability.Checked = True Then
                htData("Availability") = 1
            Else
                htData("Availability") = 0
            End If
            htData("Floor") = txtFloor.Text
            htData("Description") = txtDescription.Text

            Dim oRoomController As RoomController = New RoomController
            oRoomController.insert(htData)

        Else
            ' Do nothing in this case
        End If

    End Sub

    'validate the form
    Private Function validateFormData() As Boolean

        Dim oValidation As New Validation
        Dim bIsValid As Boolean
        Dim bAllFieldsValid As Boolean
        bAllFieldsValid = True
        Dim tt As New ToolTip()

        'validate room number
        bIsValid = IsNumeric(txtRoomNumber.Text)
        If bIsValid Then
            picErrorRoomNumber.Visible = False
        Else
            picErrorRoomNumber.Visible = True
            tt.SetToolTip(picErrorRoomNumber, "Value is not numeric")
            bAllFieldsValid = False
        End If

        'validate type
        bIsValid = oValidation.isAlphaNumericVal(txtType.Text)
        If bIsValid Then
            picErrorType.Visible = False
        Else
            picErrorType.Visible = True
            tt.SetToolTip(picErrorType, "Value is not alphanumeric")
            bAllFieldsValid = False
        End If

        'validate price
        bIsValid = IsNumeric(txtPrice.Text)
        If bIsValid Then
            picErrorPrice.Visible = False
        Else
            picErrorNumBeds.Visible = True
            tt.SetToolTip(picErrorNumBeds, "Value is not numeric")
            bAllFieldsValid = False
        End If

        'validate number bedroom
        bIsValid = oValidation.isAlphaNumericVal(txtNumBeds.Text)
        If bIsValid Then
            picErrorNumBeds.Visible = False
        Else
            picErrorNumBeds.Visible = True
            tt.SetToolTip(picErrorNumBeds, "Value is not alphanumeric")
            bAllFieldsValid = False
        End If

        'validate floor
        bIsValid = IsNumeric(txtFloor.Text)
        If bIsValid Then
            picErrorFloor.Visible = False
        Else
            picErrorFloor.Visible = True
            tt.SetToolTip(picErrorFloor, "Value is not numeric")
            bAllFieldsValid = False
        End If

        'validate description
        bIsValid = oValidation.isAlphaNumericVal(txtDescription.Text)
        If bIsValid Then
            picErrorDescription.Visible = False
        Else
            picErrorDescription.Visible = True
            tt.SetToolTip(picErrorDescription, "Value is not alphanumeric")
            bAllFieldsValid = False
        End If

        If bAllFieldsValid Then
            Debug.Print("All fields are valid")
        Else
            MsgBox("One of the fields was invalid")
        End If

        Return bAllFieldsValid

    End Function

    'button find
    Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click

        Dim oController As RoomController = New RoomController

        Dim sId = txtRoomID.Text
        Dim lsData = oController.findById(sId)

        If lsData.Count = 1 Then
            populateFormFields(lsData.Item(0))
        Else
            Debug.Print("No records were found")
        End If

    End Sub

    'populate data for button find
    Private Sub populateFormFields(htdata As Hashtable)

        txtRoomID.Text = CStr(htdata("RoomID"))
        txtRoomNumber.Text = CStr(htdata("RoomNumber"))
        txtType.Text = CStr(htdata("Type"))
        txtPrice.Text = CStr(htdata("Price"))
        txtNumBeds.Text = CStr(htdata("NumBeds"))
        If CInt(htdata("Availability")) = 1 Then
            chkAvailability.Checked = True
        Else
            chkAvailability.Checked = False
        End If
        txtFloor.Text = CStr(htdata("Floor"))
        txtDescription.Text = CStr(htdata("Description"))
    End Sub

    'button update'
    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim oController As RoomController = New RoomController

        Dim bIsValid = validateFormData()

        If bIsValid Then
            Dim htData As Hashtable = New Hashtable
            htData("RoomID") = txtRoomID.Text
            htData("RoomNumber") = txtRoomNumber.Text
            htData("Type") = txtType.Text
            htData("Price") = txtPrice.Text
            htData("NumBeds") = txtNumBeds.Text
            If chkAvailability.Checked = True Then
                htData("Availability") = 1
            Else
                htData("Availability") = 0
            End If
            htData("Floor") = txtFloor.Text
            htData("Description") = txtDescription.Text

            Dim iNumRows = oController.update(htData)

            If iNumRows = 1 Then
                Debug.Print("The record was updated. Use the find button to check ...")
            Else
                Debug.Print("The record was not updated!")
            End If

        Else
            'nothing to do here
        End If

    End Sub

    'button delete'
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim oController As RoomController = New RoomController
        Dim sId = txtRoomID.Text
        Dim iNumRows = oController.delete(sId)

        If MsgBox("Are you Sure?", MsgBoxStyle.YesNoCancel, "Title") = MsgBoxResult.Yes Then
            If iNumRows = 1 Then
                clearForm()
                Debug.Print("The record was deleted. Use the find button to check ...")
            Else
                Debug.Print("The record was not deleted!")
            End If
        End If

    End Sub

    'clear everything on the form
    Private Sub clearForm()
        txtRoomID.Clear()
        txtRoomNumber.Clear()
        txtType.Clear()
        txtPrice.Clear()
        txtNumBeds.Clear()
        chkAvailability.Checked = False
        txtFloor.Clear()
        txtDescription.Clear()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        clearForm()
    End Sub

    'our initial value for our minimun value to be used later for navigation
    Public minval As Integer = 0
    'the integer thata will hold the maximum value
    Public maxval As Integer
    'init lsNavData as all data for navigation
    Public lsNavData As New List(Of Hashtable)

    Private Sub frmRoom_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim oController As RoomController = New RoomController
        Dim lsData = oController.firstRecord()

        If lsData.Count = 1 Then
            populateFormFields(lsData.Item(0))
        Else
            'do nothing
        End If

        'find all record for navigation
        Dim lsNavData = oController.findAll()
        maxval = lsNavData.Count()

    End Sub

    'next record
    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        Dim oController As RoomController = New RoomController
        Dim lsNavData = oController.findAll()

        If minval <> maxval - 1 Then
            'minval is incremented by 1
            minval = minval + 1
            'throw the value of minval to nav procedure
            populateFormFields(lsNavData.Item(minval))

        Else
            MsgBox("Last record!")
        End If

    End Sub

    'prev record
    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrev.Click

        Dim oController As RoomController = New RoomController
        Dim lsNavData = oController.findAll()

        'check if minval is still greater than zero
        If minval > 0 Then
            'minval will be decremented by one
            minval = minval - 1
            populateFormFields(lsNavData.Item(minval))

        ElseIf minval = -1 Then
            MsgBox("No results found!")

        ElseIf minval = 0 Then
            MsgBox("First Record")
            populateFormFields(lsNavData.Item(minval))
        End If

    End Sub

    'first record
    Private Sub btnFirst_Click(sender As Object, e As EventArgs) Handles btnFirst.Click
        Dim oController As RoomController = New RoomController
        Dim lsNavData = oController.findAll()

        If minval <> 0 Then
            minval = 0
            populateFormFields(lsNavData.Item(minval))
        End If

    End Sub

    'last record
    Private Sub btnLast_Click(sender As Object, e As EventArgs) Handles btnLast.Click

        Dim oController As RoomController = New RoomController
        Dim lsNavData = oController.findAll()

        If minval <> maxval Then
            minval = maxval - 1
            populateFormFields(lsNavData.Item(minval))
        End If

    End Sub

End Class
